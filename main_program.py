from flask import Flask, render_template, request, render_template_string, redirect, url_for, send_file
from wtforms import Form, FloatField, IntegerField, FormField, SubmitField, SelectField, BooleanField, HiddenField, \
    FileField, StringField
from flask_wtf import FlaskForm
from wtforms.fields.html5 import DateField
from common import cache
from make_ppt import *

c_config = {"CINB": "#12284B",
            "CICy": "#0099D6",
            "CIGr": "#00B189",
            "CIYe": "#FAA216",
            "CIRe": "#F04C24"}

app = Flask(__name__)
cache.init_app(app=app, config={"CACHE_TYPE": "filesystem", 'CACHE_DIR': '/tmp'})

app.secret_key = '?'

SITE_LIST = ['Bogalusa RB21', 'Campti RB3', 'Columbus RB1', 'Eastover RB1', 'FlintRiver RB1', 'Franklin RB6',
             'Georgetown RB1', 'Georgetown RB2', 'Mansfield RB1', 'Mansfield RB2', 'Orange RB1', 'Orange RB2',
             'Pensacola RB1', 'Pensacola RB2', 'Pinehill RB2', 'Prattville RB1', 'Riegelwood RB4', 'Riverdale RB1',
             'Riverdale RB2', 'Rome RB5', 'Saillat RB1', 'Springfield RB4', 'Svetogorsk RB1', 'Svetogorsk RB3',
             'Ticonderoga RB1', 'Valliant RB2', 'Vicksburg RB1']

REPORT_LIST = ['All', 'Steam & Ops', 'Performance', 'Zones & Rems', 'Flow (SD)', 'Flow (Ops)', 'SFD', 'Amps']


class DateForm(FlaskForm):
    start_date = DateField('DatePicker')
    end_date = DateField('DatePicker')
    submit_button = SubmitField('Select Dates')


def get_send_obj(byte_obj):
    return send_file(byte_obj,
                     attachment_filename='plot.png',
                     mimetype='image/png')


@app.route('/', methods=["GET", "POST"])
def home():
    return render_template('home.html', site_list=SITE_LIST)


@app.route('/date/<site>', methods=["GET", "POST"])
def date(site: str):
    dform = DateForm()
    if request.method == "POST":
        dform.populate_obj(request)
        if dform.is_submitted():
            this_start_date = dform.start_date.data
            this_end_date = dform.end_date.data
            if this_start_date is None or this_end_date is None:
                return render_template('date.html', dform=dform, site=site)
            this_start_date = dt.datetime.strftime(this_start_date, "%m-%d-%Y")
            this_end_date = dt.datetime.strftime(this_end_date, "%m-%d-%Y")
            return redirect(url_for('chooser', site=site, sdate=this_start_date,
                                    edate=this_end_date))

    return render_template('date.html', dform=dform, site=site)


@app.route('/chooser/<site>dates<sdate>to<edate>', methods=["GET", "POST"])
def chooser(site, sdate, edate):
    fig_dict = {}
    this_site = site.split(' ')[0]
    this_rb = site.split(' ')[1]
    if request.method == "GET":
        if 'report_choice' in request.args:
            this_choice = request.args['report_choice']
            if this_choice in ["Steam & Ops", "All"]:
                this_ops_dict = get_ops_dict(this_site, this_rb, sdate, edate)
                this_plots = get_stm_ops_plot(this_site, this_ops_dict, sdate, edate)
                fig_dict["Steam & Ops"] = this_plots

            if this_choice in ["Performance", "All"]:
                this_perf_dict = get_perf_dict(this_site, this_rb, sdate, edate)
                this_plots = get_perf_plots(this_perf_dict, this_site, this_rb)
                fig_dict["Performance"] = this_plots

            if this_choice in ["Zones & Rems", "All"]:
                this_zone_dict, this_z_config = get_zones_dict(this_site, this_rb, sdate, edate)
                this_zce_dict = get_zce_dict(this_site, this_z_config, this_rb, sdate, edate)
                this_plots = get_zones_plots(this_zce_dict, this_zone_dict, this_z_config, sdate, edate)
                fig_dict["Zones & Removals"] = this_plots

            if this_choice in ["Flow (SD)", "Flow (Ops)", "All"]:
                this_flow_dict = get_flow_dict(this_site, this_rb, sdate, edate)
                if "SD" in this_choice:
                    this_opt = 'sd'
                else:
                    this_opt = 'ops'
                this_plots = get_flow_plots(this_flow_dict, this_opt, this_site, this_rb)
                fig_dict["Flows"] = this_plots

            if this_choice in ["Amps", "All"]:
                this_amp_dict = get_amp_dict(this_site, this_rb, sdate, edate)
                this_plots = get_amp_plots(this_amp_dict, None, this_site, this_rb)
                fig_dict["Current"] = this_plots

            return render_template('reports.html', site=site, sdate=sdate, edate=edate, report=this_choice,
                                   fig_dict=fig_dict)

    return render_template("chooser.html", site=site, sdate=sdate, edate=edate, report_list=REPORT_LIST,
                           fig_dict=fig_dict)


@app.route('/reports/<site>dates<sdate>to<edate><report>', methods=["GET", "POST"])
def reports(site, sdate, edate, report, fig_list, nav_dict):
    print(nav_dict)
    return render_template('reports.html', site=site, sdate=sdate, edate=edate, report=report, fig_list=fig_list,
                           nav_dict=nav_dict)

@app.route('/test/')
def test():
    test_dict = {1: {2: 3},
                 4: {5: 6},
                 7: {8: 9}}
    return render_template('test.html', test_dict=test_dict)


if __name__ == '__main__':
    app.run(debug=True)
