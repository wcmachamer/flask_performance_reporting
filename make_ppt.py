import pandas as pd
import os
import numpy as np
import datetime as dt
from dateutil.parser import parse
import base64

from matplotlib import pyplot as plt
from matplotlib import dates as mdates
from matplotlib.dates import MO, TU, WE, TH, FR, SA, SU
from matplotlib import gridspec
from matplotlib import patches as mpatches
from matplotlib.figure import Figure
import mpld3
from cycler import cycler
import requests
import urllib3
import copy
import io

import pptx

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
plt.ioff()
this_wd = {1: MO,
           2: TU,
           3: WE,
           4: TH,
           5: FR,
           6: SA,
           7: SU}
c_config = {"CINB": "#12284B",
            "CICy": "#0099D6",
            "CIGr": "#00B189",
            "CIYe": "#FAA216",
            "CIRe": "#F04C24"}
custom_cycler = (cycler(color=[c_config["CINB"], c_config["CICy"], c_config["CIGr"], c_config["CIYe"], c_config["CIRe"],
                               "#245096", "#00BFFF", "#00DEAC", "#FAD746", "#F06432", c_config["CINB"],
                               c_config["CICy"], c_config["CIGr"], c_config["CIYe"], c_config["CIRe"],
                               "#245096", "#00BFFF", "#00DEAC", "#FAD746", "#F06432"]))
site_db = {"Bogalusa": "IP",
           "Campti": "IP",
           "Columbus": "IP",
           "Eastover": "IP",
           "FlintRiver": "IP",
           "Franklin": "IP",
           "Georgetown": "IP",
           "Mansfield": "IP",
           "Orange": "IP",
           "Pensacola": "IP",
           "PineHill": "IP",
           "Prattville": "IP",
           "Riegelwood": "IP",
           "Riverdale": "IP",
           "Rome": "IP",
           "Saillat": "IP",
           "Springfield": "IP",
           "Svetogorsk": "IP",
           "Ticonderoga": "IP",
           "Valliant": "IP",
           "Vicksburg": "IP",
           "Klabin": "Pulp_Paper",
           "Lewiston": "Pulp_Paper",
           "NorthCharleston": "Pulp_Paper",
           "RoanokeRapids": "Pulp_Paper",
           "Texarkana": "Pulp_Paper"}
piwebapi_user = (r'ci\svc_PIAF_cb', 'Cbatadmin@01')
piwebapi_base_srch = "https://am01-01-sv-503/piwebapi/"


def check_for_site_folder(this_site, this_rb, start_date, end_date):
    if not os.path.isdir(this_dir + "\\" + this_site):
        os.makedirs(this_dir + "\\" + this_site)
    print("Beginning gathering data from " + this_site)


def get_ops_dict(this_site, this_rb, start_date, end_date):
    """Gathers operation type and count from SBE element"""
    # Get sbe data from api
    this_end_date = parse(end_date)
    this_start_date = parse(start_date)
    this_num_ds = (this_end_date - this_start_date).days
    this_ds = [this_start_date + dt.timedelta(days=x) for x in range(this_num_ds)]
    this_db = site_db[this_site]
    this_srch_str = r"search/query?q=name:SBE&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site
    this_srch = requests.get(piwebapi_base_srch + this_srch_str, auth=piwebapi_user, verify=False).json()
    # if len(this_srch["Errors"]) > 0:
    #     print(this_srch["Errors"][0]["Message"])
    #     return
    this_ops_dict = {}
    for this_sbe in this_srch["Items"]:
        this_sbe_ele = requests.get(this_sbe["Links"]["Self"],
                                    verify=False,
                                    auth=piwebapi_user).json()
        this_rb = this_sbe_ele["Path"].split("\\")[5]
        print("Gathering operational data from " + this_site + " " + this_rb + " SBE Element.")
        this_ops_dict[this_rb] = {}

        for this_day in this_ds:
            this_ops_dict[this_rb][this_day] = {"Zone": 0,
                                                "Timer": 0,
                                                "Rule": 0,
                                                "Man": 0,
                                                "OneWay": 0}

        if "BlowSelection" in [x["Name"] for x in this_srch["Items"][0]["Attributes"]]:
            this_atts = [x for x in this_sbe["Attributes"] if x["Name"] in ["StartTime", "BlowSelection", "BlowType"]]
            this_ts_webid = [x["WebId"] for x in this_atts if x["Name"] == "StartTime"][0]
            this_ts_src_str = "streams/" + this_ts_webid + "/recorded?StartTime=" + start_date + \
                              "&EndTime=" + end_date + "&maxcount=100000"
            this_ts = requests.get(piwebapi_base_srch + this_ts_src_str, auth=piwebapi_user, verify=False).json()[
                "Items"]
            this_ts = [parse(x["Value"]) for x in this_ts]

            this_bs_webid = [x["WebId"] for x in this_atts if x["Name"] == "BlowSelection"][0]
            this_bs_src_str = "streams/" + this_bs_webid + "/recorded?StartTime=" + start_date + \
                              "&EndTime=" + end_date + "&maxcount=100000"
            this_bs_names = requests.get(piwebapi_base_srch + this_bs_src_str, auth=piwebapi_user, verify=False).json()[
                "Items"]
            this_bt_webid = [x["WebId"] for x in this_atts if x["Name"] == "BlowType"][0]
            this_bt_src_str = "streams/" + this_bt_webid + "/recorded?StartTime=" + start_date + \
                              "&EndTime=" + end_date + "&maxcount=100000"
            this_bt_names = requests.get(piwebapi_base_srch + this_bt_src_str, auth=piwebapi_user, verify=False).json()[
                "Items"]

            for this_bt in this_bt_names:
                if this_bt["Value"] is None:
                    this_bt["Value"] = {"Name": "Two"}
            if this_site in ["Prattville", "PineHill", "Svetogorsk"]:
                for this_day in sorted(this_ds):
                    this_date = this_day.date()
                    this_day_ind = [ind for ind, x in enumerate(this_ts) if x.date() == this_date]
                    this_ops_dict[this_rb][this_day]["Zone"] = len(
                        [x for x in this_day_ind if "ISB" in this_bs_names[x]["Value"].upper() and
                         "NON" not in this_bs_names[x]["Value"].upper()])
                    this_ops_dict[this_rb][this_day]["Timer"] = len(
                        [x for x in this_day_ind if "TIMER" in this_bs_names[x]["Value"].upper()])
                    this_ops_dict[this_rb][this_day]["Rule"] = len(
                        [x for x in this_day_ind if "RULE" in this_bs_names[x]["Value"].upper()])
                    this_ops_dict[this_rb][this_day]["Man"] = len(
                        [x for x in this_day_ind if "NON" in this_bs_names[x]["Value"].upper()])
                    #               Prattville, Pinehill have no one-way blowers
                    this_ops_dict[this_rb][this_day]["OneWay"] = 0
            if this_site in ["Eastover", "Springfield", "Ticonderoga"]:
                for this_day in sorted(this_ds):
                    this_date = this_day.date()
                    this_day_ind = [ind for ind, x in enumerate(this_ts) if x.date() == this_date]
                    this_ops_dict[this_rb][this_day]["Zone"] = len(
                        [x for x in this_day_ind if "isb" in this_bs_names[x]["Value"].lower() and
                         "non" not in this_bs_names[x]["Value"].lower()])
                    this_ops_dict[this_rb][this_day]["Timer"] = len(
                        [x for x in this_day_ind if "timer" in this_bs_names[x]["Value"].lower()])
                    this_ops_dict[this_rb][this_day]["Rule"] = len(
                        [x for x in this_day_ind if "rule" in this_bs_names[x]["Value"].lower()])
                    this_ops_dict[this_rb][this_day]["Man"] = len(
                        [x for x in this_day_ind if "non" in this_bs_names[x]["Value"].lower()])
                    try:
                        this_ops_dict[this_rb][this_day]["One-Way"] = len(
                            [x for x in this_day_ind if "one" in this_bt_names[x]["Value"]["Name"].lower()])
                    except:
                        this_ops_dict[this_rb][this_day]["One-Way"] = len(
                            [x for x in this_day_ind[:-1] if "one" in this_bt_names[x]["Value"]["Name"].lower()])


        else:
            this_atts = [x for x in this_sbe["Attributes"] if
                         x["Name"].lower() in ["starttime", "zonename", "rulename", "selectedbysc", "blowtype"]]
            this_z_webid = [x["WebId"] for x in this_atts if x["Name"] == "ZoneName"][0]
            this_z_src_str = "streams/" + this_z_webid + "/recorded?StartTime=" + start_date + \
                             "&EndTime=" + end_date + "&maxcount=100000"
            this_z_names = requests.get(piwebapi_base_srch + this_z_src_str, auth=piwebapi_user, verify=False).json()[
                "Items"]
            this_r_webid = [x["WebId"] for x in this_atts if x["Name"] == "RuleName"][0]
            this_r_src_str = "streams/" + this_r_webid + "/recorded?StartTime=" + start_date + \
                             "&EndTime=" + end_date + "&maxcount=100000"
            this_r_names = requests.get(piwebapi_base_srch + this_r_src_str, auth=piwebapi_user, verify=False).json()[
                "Items"]
            this_sc_webid = [x["WebId"] for x in this_atts if x["Name"] == "SelectedBySC"][0]
            this_sc_src_str = "streams/" + this_sc_webid + "/recorded?StartTime=" + start_date + \
                              "&EndTime=" + end_date + "&maxcount=100000"
            this_sc_sel = requests.get(piwebapi_base_srch + this_sc_src_str, auth=piwebapi_user, verify=False).json()[
                "Items"]
            this_ts_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "starttime"][0]
            this_ts_src_str = "streams/" + this_ts_webid + "/recorded?StartTime=" + start_date + \
                              "&EndTime=" + end_date + "&maxcount=100000"
            this_times = requests.get(piwebapi_base_srch + this_ts_src_str, auth=piwebapi_user, verify=False).json()[
                "Items"]
            this_bt_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "blowtype"][0]
            this_bt_src_str = "streams/" + this_bt_webid + "/recorded?StartTime=" + start_date + \
                              "&EndTime=" + end_date + "&maxcount=100000"
            this_bts = requests.get(piwebapi_base_srch + this_bt_src_str, auth=piwebapi_user, verify=False).json()[
                "Items"]

            this_z_names_dict = {"vals": [x["Value"] for x in this_z_names],
                                 'tds': [parse(x["Timestamp"]) for x in this_z_names]}

            this_sc_sel_dict = {"vals": [x["Value"] for x in this_sc_sel],
                                'tds': [parse(x["Timestamp"]) for x in this_sc_sel]}
            if this_site in ["Columbus"]:
                this_epoch = parse('1/1/2010')
                this_times_dict = {"vals": [this_epoch + dt.timedelta(seconds=x['Value']) for x in this_times],
                                   'tds': [parse(x["Timestamp"]) for x in this_times]}
            else:
                this_times_dict = {"vals": [parse(x["Value"]) for x in this_times],
                                   'tds': [parse(x["Timestamp"]) for x in this_times]}
            this_bts_dict = {"vals": [x["Value"] for x in this_bts],
                             'tds': [parse(x["Timestamp"]) for x in this_bts]}
            if this_site == "Georgetown":
                if len(this_z_names_dict["vals"]) < len(this_times_dict["vals"]):
                    this_len = len(this_times_dict["tds"])
                    this_new_vals = [""] * this_len
                    this_new_tds = this_times_dict["tds"]
                    this_ind = 0
                    for this_td in this_new_tds:
                        if this_td in this_z_names_dict["tds"]:
                            this_new_ind = [i for i, x in enumerate(this_z_names_dict["tds"])][0]
                            this_new_vals[this_ind] = this_z_names_dict["vals"][this_new_ind]
                        else:
                            this_new_vals[this_ind] = ""
                        this_ind += 1
                this_z_names_dict["vals"] = this_new_vals
                this_z_names_dict["tds"] = this_new_tds
                this_r_names_dict = {"vals": [""] * len(this_z_names_dict["vals"]),
                                     "tds": this_z_names_dict["tds"]}

            else:
                this_r_names_dict = {"vals": [x["Value"] for x in this_r_names],
                                     'tds': [parse(x["Timestamp"]) for x in this_r_names]}
            if this_site in ["Columbus"]:
                this_new_z_vals = ['' if x == 0 else "WT{:02n}".format(int(x)) for x in this_z_names_dict['vals']]
                this_z_names_dict['vals'] = this_new_z_vals
                this_new_sc_sel_val = ["True" if x == 1 else "False" for x in this_sc_sel_dict["vals"]]
                this_sc_sel_dict["vals"] = this_new_sc_sel_val
                this_new_r_vals = ['' if x == 0 else "WT{:02n}".format(int(x)) for x in this_r_names_dict['vals']]
                this_r_names_dict['vals'] = this_new_r_vals


            for this_day in this_ds:
                this_day_ind = [i for i, x in enumerate(this_times_dict["tds"]) if x.date() == this_day.date()]
                this_ops_dict[this_rb][this_day]["Zone"] = len(
                    [x for x in this_day_ind if this_z_names_dict["vals"][x].strip() != ''])
                this_ops_dict[this_rb][this_day]["Timer"] = len(
                    [x for x in this_day_ind if this_z_names_dict["vals"][x].strip() == '' and
                     this_sc_sel_dict["vals"][x].strip() == 'True'])
                this_ops_dict[this_rb][this_day]["Rule"] = len(
                    [x for x in this_day_ind if this_r_names_dict["vals"][x].strip() != ''])
                this_ops_dict[this_rb][this_day]["Man"] = len(
                    [x for x in this_day_ind if this_sc_sel_dict["vals"][x].strip() == 'False'])
                this_ops_dict[this_rb][this_day]["OneWay"] = len(
                    [x for x in this_day_ind if "one" in this_bts_dict['vals'][x].lower()])

    return this_ops_dict


def get_zones_dict(this_site, this_rb, start_date, end_date):
    """Gets hourly zone PVs and SB assignment per zone"""
    print("Gathering Zone PVs...")
    this_db = site_db[this_site]
    this_zones_srch_str = r"search/query?q=name:Zones&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site
    this_zones_srch = requests.get(piwebapi_base_srch + this_zones_srch_str,
                                   auth=piwebapi_user,
                                   verify=False).json()['Items']
    this_z_dict = {}
    this_z_config = {}
    for this_z_ele in this_zones_srch:
        this_zones_ele = requests.get(this_z_ele["Links"]["Self"],
                                      auth=piwebapi_user,
                                      verify=False).json()
        this_zones_list = requests.get(this_zones_ele["Links"]["Elements"],
                                       auth=piwebapi_user,
                                       verify=False).json()["Items"]
        this_rb = this_zones_ele["Path"].split("\\")[5]
        this_z_dict[this_rb] = {}
        this_z_config[this_rb] = {}
        print("Working on " + this_site + " " + this_rb + "...")
        if "SC1.0" in this_zones_list[0]["TemplateName"]:
            for this_zone in this_zones_list:
                print("Gathering data for " + this_zone["Name"])
                this_atts = requests.get(this_zone["Links"]["Attributes"],
                                         auth=piwebapi_user,
                                         verify=False).json()["Items"]
                this_pv_webid = [x["WebId"] for x in this_atts if x["Name"] == "PV"][0]
                this_pv_srch_str = "streams/" + this_pv_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                                   + end_date + "&summaryDuration=1h&SummaryType=Average"
                this_pv_vals = requests.get(piwebapi_base_srch + this_pv_srch_str,
                                            verify=False,
                                            auth=piwebapi_user).json()["Items"]
                this_yrly_strt =parse(end_date) - dt.timedelta(days=365)
                this_yrly_pv_str = "streams/" + this_pv_webid + "/summary?StartTime=" + str(
                    this_yrly_strt) + "&EndTime=" + end_date + "&summaryDuration=1d&SummaryType=Average"
                this_yrly_pv_vals = requests.get(piwebapi_base_srch + this_yrly_pv_str,
                                                 verify=False,
                                                 auth=piwebapi_user).json()["Items"]
                this_csp_webid = [x["WebId"] for x in this_atts if x["Name"] == "Clean_SP"][0]
                this_csp_srch_str = "streams/" + this_csp_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                                    + end_date + "&summaryDuration=1h&SummaryType=Average"
                this_csp_vals = requests.get(piwebapi_base_srch + this_csp_srch_str,
                                             verify=False,
                                             auth=piwebapi_user).json()["Items"]
                this_dsp_webid = [x["WebId"] for x in this_atts if x["Name"] == "Dirty_SP"][0]
                this_dsp_srch_str = "streams/" + this_dsp_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                                    + end_date + "&summaryDuration=1h&SummaryType=Average"
                this_dsp_vals = requests.get(piwebapi_base_srch + this_dsp_srch_str,
                                             verify=False,
                                             auth=piwebapi_user).json()["Items"]

                this_pv_vals_list = [x["Value"]["Value"] for x in this_pv_vals]
                this_pv_tds_list = [parse(x["Value"]["Timestamp"])
                                    for x in this_pv_vals]
                this_csp_vals_list = [x["Value"]["Value"] for x in this_csp_vals]
                this_csp_tds_list = [parse(x["Value"]["Timestamp"])
                                     for x in this_csp_vals]
                this_dsp_vals_list = [x["Value"]["Value"] for x in this_dsp_vals]
                this_dsp_tds_list = [parse(x["Value"]["Timestamp"])
                                     for x in this_dsp_vals]
                this_yrly_pv_list = [x["Value"]["Value"] for x in this_yrly_pv_vals]
                this_yrly_td_list = [parse(x["Value"]["Timestamp"])
                                     for x in this_yrly_pv_vals]

                this_z_dict[this_rb][this_zone["Name"]] = {"PV": {"vals": this_pv_vals_list,
                                                                  "tds": this_pv_tds_list},
                                                           "Clean_SP": {"vals": this_csp_vals_list,
                                                                        "tds": this_csp_tds_list},
                                                           "Dirty_SP": {"vals": this_dsp_vals_list,
                                                                        "tds": this_dsp_tds_list},
                                                           "YR_PV": {"vals": this_yrly_pv_list,
                                                                     "tds": this_yrly_td_list}}
                this_id_webid = [x["WebId"] for x in this_atts if x["Name"] == "ID"][0]
                this_id_srch_str = "streams/" + this_id_webid + "/recorded?StartTime=01/01/1970&maxcount=1"
                this_id_val = requests.get(piwebapi_base_srch + this_id_srch_str,
                                           verify=False,
                                           auth=piwebapi_user).json()["Items"][0]["Value"]
                this_z_config[this_rb][int(this_id_val)] = {"Name": this_zone["Name"]}
                this_z_ce_search = requests.get(this_zone["Links"]["Elements"],
                                                verify=False,
                                                auth=piwebapi_user).json()["Items"]
                this_z_sblist = [x["Name"] for x in this_z_ce_search if "Sootblowers" in x["Path"]]
                this_z_config[this_rb][int(this_id_val)]["sbs"] = this_z_sblist
        if "SC2.0" in this_zones_list[0]["TemplateName"]:
            for this_zone in this_zones_list:
                print("Gathering data for " + [this_zone["Name"]][0])
                this_atts = requests.get(this_zone["Links"]["Attributes"],
                                         auth=piwebapi_user,
                                         verify=False).json()["Items"]
                this_pv_webid = [x["WebId"] for x in this_atts if x["Name"] == "PV"][0]
                this_pv_srch_str = "streams/" + this_pv_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                                   + end_date + "&summaryDuration=1h&SummaryType=Average"
                this_pv_vals = requests.get(piwebapi_base_srch + this_pv_srch_str,
                                            verify=False,
                                            auth=piwebapi_user).json()["Items"]
                this_yrly_strt = parse(end_date) - dt.timedelta(days=365)
                this_yrly_pv_str = "streams/" + this_pv_webid + "/summary?StartTime=" + str(
                    this_yrly_strt) + "&EndTime=" + end_date + "&summaryDuration=1d&SummaryType=Average"
                this_yrly_pv_vals = requests.get(piwebapi_base_srch + this_yrly_pv_str,
                                                 verify=False,
                                                 auth=piwebapi_user).json()["Items"]
                this_acc_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "acc"][0]
                this_acc_srch_str = "streams/" + this_acc_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                                    + end_date + "&summaryDuration=1h&SummaryType=Average"
                this_acc_vals = requests.get(piwebapi_base_srch + this_acc_srch_str,
                                             verify=False,
                                             auth=piwebapi_user).json()["Items"]
                this_nbr_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "nbr"][0]
                this_nbr_srch_str = "streams/" + this_nbr_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                                    + end_date + "&summaryDuration=1h&SummaryType=Average"
                this_nbr_vals = requests.get(piwebapi_base_srch + this_nbr_srch_str,
                                             verify=False,
                                             auth=piwebapi_user).json()["Items"]

                this_pv_vals_list = [x["Value"]["Value"] for x in this_pv_vals]
                this_pv_tds_list = [parse(x["Value"]["Timestamp"])
                                    for x in this_pv_vals]
                this_acc_vals_list = [x["Value"]["Value"] for x in this_acc_vals]
                this_acc_tds_list = [parse(x["Value"]["Timestamp"])
                                     for x in this_acc_vals]
                this_nbr_vals_list = [x["Value"]["Value"] for x in this_nbr_vals]
                this_nbr_tds_list = [parse(x["Value"]["Timestamp"])
                                     for x in this_nbr_vals]
                this_yrly_pv_list = [x["Value"]["Value"] for x in this_yrly_pv_vals]
                this_yrly_td_list = [parse(x["Value"]["Timestamp"])
                                     for x in this_yrly_pv_vals]

                this_z_dict[this_rb][this_zone["Name"]] = {"PV": {"vals": this_pv_vals_list,
                                                                  "tds": this_pv_tds_list},
                                                           "ACC": {"vals": this_acc_vals_list,
                                                                   "tds": this_acc_tds_list},
                                                           "NBR": {"vals": this_nbr_vals_list,
                                                                   "tds": this_nbr_tds_list},
                                                           "YR_PV": {"vals": this_yrly_pv_list,
                                                                     "tds": this_yrly_td_list}}
                this_id_webid = [x["WebId"] for x in this_atts if x["Name"] == "ID"][0]
                this_id_srch_str = "streams/" + this_id_webid + "/recorded?StartTime=01/01/1970&maxcount=1"
                this_id_val = requests.get(piwebapi_base_srch + this_id_srch_str,
                                           verify=False,
                                           auth=piwebapi_user).json()["Items"][0]["Value"]
                this_z_config[this_rb][int(this_id_val)] = {"Name": this_zone["Name"]}
                this_z_ce_search = requests.get(this_zone["Links"]["Elements"],
                                                verify=False,
                                                auth=piwebapi_user).json()["Items"]
                this_z_sblist = [x["Name"] for x in this_z_ce_search if "Sootblowers" in x["Path"]]
                this_z_config[this_rb][int(this_id_val)]["sbs"] = this_z_sblist

    return this_z_dict, this_z_config


def get_amp_dict(this_site, this_rb, start_date, end_date):
    this_db = site_db[this_site]
    this_sb_srch_str = r"search/query?q=name:Sootblowers&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site
    this_sbs_ele = requests.get(piwebapi_base_srch + this_sb_srch_str,
                                verify=False,
                                auth=piwebapi_user).json()["Items"]
    this_sb_dict = {}
    for this_ele in this_sbs_ele:
        this_sbs = requests.get(this_ele["Links"]["Self"],
                                verify=False,
                                auth=piwebapi_user).json()
        this_rb = this_sbs["Path"].split('\\')[5]
        this_sb_list = requests.get(this_sbs["Links"]["Elements"],
                                    verify=False,
                                    auth=piwebapi_user).json()["Items"]
        this_att_check = requests.get(this_sb_list[0]["Links"]["Attributes"],
                                      verify=False,
                                      auth=piwebapi_user).json()["Items"]
        this_amp_check = len([x for x in this_att_check if x["Name"].lower() == "insert_avg_amps"])
        this_sb_dict[this_rb] = {}
        print("Gathering current data for " + this_site + " " + this_rb)

        if this_amp_check > 0:
            for this_sb in this_sb_list:
                this_sb_name = this_sb["Name"]
                this_atts = requests.get(this_sb["Links"]["Attributes"],
                                         verify=False,
                                         auth=piwebapi_user).json()["Items"]

                this_sec_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "section"][0]
                this_sec_srch_str = "streams/" + this_sec_webid + "/recorded?StartTime=01/01/1970"
                this_sec_srch = requests.get(piwebapi_base_srch + this_sec_srch_str,
                                             verify=False,
                                             auth=piwebapi_user).json()["Items"]
                this_sec = this_sec_srch[-1]["Value"]

                this_side_att = [x for x in this_atts if x["Name"].lower() == "side"][0]
                this_side_webid = this_side_att["WebId"]
                this_side_srch_str = "streams/" + this_side_webid + "/recorded?StartTime=01/01/1970"
                this_side_srch = requests.get(piwebapi_base_srch + this_side_srch_str,
                                              verify=False,
                                              auth=piwebapi_user).json()["Items"]

                if len(this_side_srch) > 0:
                    this_side = this_side_srch[0]["Value"]
                else:
                    this_side = requests.get(this_side_att["Links"]["Value"],
                                             auth=piwebapi_user,
                                             verify=False).json()["Value"]
                if this_sec not in list(this_sb_dict[this_rb].keys()):
                    this_sb_dict[this_rb][this_sec] = {this_side: {this_sb_name: {"ins": [],
                                                                                  "op": []}}}
                elif this_side not in list(this_sb_dict[this_rb][this_sec].keys()):
                    this_sb_dict[this_rb][this_sec][this_side] = {this_sb_name: {"ins": [],
                                                                                 "op": []}}
                else:
                    this_sb_dict[this_rb][this_sec][this_side][this_sb_name] = {"ins": [],
                                                                                "op": []}

                this_ia_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "insert_avg_amps"][0]
                this_ia_srch_str = "streams/" + this_ia_webid + "/recorded?StartTime=" + start_date + \
                                   "&EndTime=" + end_date + "&uom=pph&maxcount=100000"
                this_ia_srch = requests.get(piwebapi_base_srch + this_ia_srch_str,
                                            verify=False,
                                            auth=piwebapi_user).json()["Items"]
                this_op_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "op_avg_amps"][0]
                this_op_srch_str = "streams/" + this_op_webid + "/recorded?StartTime=" + start_date + \
                                   "&EndTime=" + end_date + "&uom=pph&maxcount=100000"
                this_op_srch = requests.get(piwebapi_base_srch + this_op_srch_str,
                                            verify=False,
                                            auth=piwebapi_user).json()["Items"]

                this_sb_dict[this_rb][this_sec][this_side][this_sb_name]["ins"] = [x["Value"] for x in this_ia_srch if
                                                                                   type(x["Value"]) == float]
                this_sb_dict[this_rb][this_sec][this_side][this_sb_name]["op"] = [x["Value"] for x in this_op_srch if
                                                                                  type(x["Value"]) == float]
        else:
            this_sb_dict[this_rb] = {}
    return this_sb_dict


def get_flow_dict(this_site, this_rb, start_date, end_date):
    this_db = site_db[this_site]
    this_sb_srch_str = r"search/query?q=name:Sootblowers&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site
    this_sbs_ele = requests.get(piwebapi_base_srch + this_sb_srch_str,
                                verify=False,
                                auth=piwebapi_user).json()["Items"]
    this_sb_dict = {}
    for this_ele in this_sbs_ele:
        this_sbs = requests.get(this_ele["Links"]["Self"],
                                verify=False,
                                auth=piwebapi_user).json()
        this_rb = this_sbs["Path"].split('\\')[5]
        this_sb_list = requests.get(this_sbs["Links"]["Elements"],
                                    verify=False,
                                    auth=piwebapi_user).json()["Items"]
        this_att_check = requests.get(this_sb_list[0]["Links"]["Attributes"],
                                      verify=False,
                                      auth=piwebapi_user).json()["Items"]
        this_flow_check = len([x for x in this_att_check if x["Name"].lower() == "insert_avg_flow"])
        this_sb_dict[this_rb] = {}
        print("Gathering flow data for " + this_site + " " + this_rb)

        if this_flow_check > 0:
            for this_sb in this_sb_list:
                this_sb_name = this_sb["Name"]
                this_atts = requests.get(this_sb["Links"]["Attributes"],
                                         verify=False,
                                         auth=piwebapi_user).json()["Items"]

                this_sec_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "section"][0]
                this_sec_srch_str = "streams/" + this_sec_webid + "/recorded?StartTime=01/01/1970"
                this_sec_srch = requests.get(piwebapi_base_srch + this_sec_srch_str,
                                             verify=False,
                                             auth=piwebapi_user).json()["Items"]
                this_sec = this_sec_srch[-1]["Value"]

                this_side_att = [x for x in this_atts if x["Name"].lower() == "side"][0]
                this_side_webid = this_side_att["WebId"]
                this_side_srch_str = "streams/" + this_side_webid + "/recorded?StartTime=01/01/1970"
                this_side_srch = requests.get(piwebapi_base_srch + this_side_srch_str,
                                              verify=False,
                                              auth=piwebapi_user).json()["Items"]
                this_sec_webid = [x for x in this_atts if x["Name"].lower() == "side"][0]

                if len(this_side_srch) > 0:
                    this_side = this_side_srch[0]["Value"]
                else:
                    this_side = requests.get(this_side_att["Links"]["Value"],
                                             auth=piwebapi_user,
                                             verify=False).json()["Value"]
                if this_sec not in list(this_sb_dict[this_rb].keys()):
                    this_sb_dict[this_rb][this_sec] = {this_side: {this_sb_name: {"flow": [],
                                                                                  "sp": []}}}
                elif this_side not in list(this_sb_dict[this_rb][this_sec].keys()):
                    this_sb_dict[this_rb][this_sec][this_side] = {this_sb_name: {"flow": [],
                                                                                 "sp": []}}
                else:
                    this_sb_dict[this_rb][this_sec][this_side][this_sb_name] = {"flow": [],
                                                                                "sp": []}

                this_flow_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "insert_avg_flow"][0]
                this_f_srch_str = "streams/" + this_flow_webid + "/recorded?StartTime=" + start_date + \
                                  "&EndTime=" + end_date + "&uom=pph&maxcount=100000"
                this_f_srch = requests.get(piwebapi_base_srch + this_f_srch_str,
                                           verify=False,
                                           auth=piwebapi_user).json()["Items"]
                if len([x["WebId"] for x in this_atts if x["Name"].lower() == "cleaning_flow_sp"]) > 0:
                    this_sp_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "cleaning_flow_sp"][0]
                    this_sp_srch_str = "streams/" + this_sp_webid + "/recorded?StartTime=01/01/1970"
                    this_sp_srch = requests.get(piwebapi_base_srch + this_sp_srch_str,
                                                verify=False,
                                                auth=piwebapi_user).json()["Items"]
                elif [x["WebId"] for x in this_atts if x["Name"].lower() == "insertflowsp"]:
                    this_sp_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "insertflowsp"][0]
                    this_sp_srch_str = "streams/" + this_sp_webid + "/recorded"
                    this_sp_srch = requests.get(piwebapi_base_srch + this_sp_srch_str,
                                                verify=False,
                                                auth=piwebapi_user).json()["Items"]

                this_sb_dict[this_rb][this_sec][this_side][this_sb_name]["flow"] = [x["Value"] for x in this_f_srch if
                                                                                    type(x["Value"]) == float]
                this_sb_dict[this_rb][this_sec][this_side][this_sb_name]["sp"] = this_sp_srch[-1]["Value"]
                if len(this_sb_dict[this_rb][this_sec][this_side][this_sb_name]["flow"]) > 0:
                    if (this_sb_dict[this_rb][this_sec][this_side][this_sb_name]["flow"][0] / this_sp_srch[0][
                        "Value"]) < 10:
                        this_sb_dict[this_rb][this_sec][this_side][this_sb_name]["flow"] = [x * 1000 for x in
                                                                                            this_sb_dict[this_rb][
                                                                                                this_sec][this_side]
                                                                                            [this_sb_name]["flow"]]
        else:
            this_sb_dict[this_rb] = {}
    return this_sb_dict


def get_sfd_dict(this_site, this_rb, start_date, end_date):
    this_db = site_db[this_site]
    this_sb_srch_str = r"search/query?q=name:Sootblowers&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site
    this_sbs_ele = requests.get(piwebapi_base_srch + this_sb_srch_str,
                                verify=False,
                                auth=piwebapi_user).json()["Items"]
    this_sb_dict = {}
    for this_ele in this_sbs_ele:
        this_sbs = requests.get(this_ele["Links"]["Self"],
                                verify=False,
                                auth=piwebapi_user).json()
        this_rb = this_sbs["Path"].split('\\')[5]
        this_sb_list = requests.get(this_sbs["Links"]["Elements"],
                                    verify=False,
                                    auth=piwebapi_user).json()["Items"]
        this_att_check = requests.get(this_sb_list[0]["Links"]["Attributes"],
                                      verify=False,
                                      auth=piwebapi_user).json()["Items"]
        this_sfd_check = len([x for x in this_att_check if x["Name"].lower() in ["sfd_priority_filtered", "sfd_fouling_index"]])
        this_sb_dict[this_rb] = {}

        if this_sfd_check > 0:
            print("Gathering SFD data for " + this_site + " " + this_rb)
            this_h_max = 0
            this_v_max = 0
            for this_sb in this_sb_list:
                this_sb_name = this_sb["Name"]
                this_atts = requests.get(this_sb["Links"]["Attributes"],
                                         verify=False,
                                         auth=piwebapi_user).json()["Items"]

                this_sec_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "section"][0]
                this_sec_srch_str = "streams/" + this_sec_webid + "/recorded?StartTime=01/01/1970"
                this_sec_srch = requests.get(piwebapi_base_srch + this_sec_srch_str,
                                             verify=False,
                                             auth=piwebapi_user).json()["Items"]
                this_sec = this_sec_srch[-1]["Value"]

                this_side_att = [x for x in this_atts if x["Name"].lower() == "side"][0]
                this_side_webid = this_side_att["WebId"]
                this_side_srch_str = "streams/" + this_side_webid + "/recorded?StartTime=01/01/1970"
                this_side_srch = requests.get(piwebapi_base_srch + this_side_srch_str,
                                              verify=False,
                                              auth=piwebapi_user).json()["Items"]

                this_hpos_att = [x for x in this_atts if x["Name"].lower() == "horizontal_location"][0]
                this_hpos_webid = this_hpos_att["WebId"]
                this_hpos_srch_str = "streams/" + this_hpos_webid + "/recorded?StartTime=01/01/1970"
                this_hpos_srch = requests.get(piwebapi_base_srch + this_hpos_srch_str,
                                              verify=False,
                                              auth=piwebapi_user).json()["Items"]
                this_vpos_att = [x for x in this_atts if x["Name"].lower() == "vertical_location"][0]
                this_vpos_webid = this_vpos_att["WebId"]
                this_vpos_srch_str = "streams/" + this_vpos_webid + "/recorded?StartTime=01/01/1970"
                this_vpos_srch = requests.get(piwebapi_base_srch + this_vpos_srch_str,
                                              verify=False,
                                              auth=piwebapi_user).json()["Items"]

                if len(this_side_srch) > 0:
                    this_side = this_side_srch[0]["Value"]
                else:
                    this_side = requests.get(this_side_att["Links"]["Value"],
                                             auth=piwebapi_user,
                                             verify=False).json()["Value"]
                if this_side not in list(this_sb_dict[this_rb].keys()):
                    this_sb_dict[this_rb][this_side] = {this_sec: {this_sb_name: {"curr": [],
                                                                                  "avg": [],
                                                                                  "hpos": 0,
                                                                                  "vpos": 0}}}
                elif this_sec not in list(this_sb_dict[this_rb][this_side].keys()):
                    this_sb_dict[this_rb][this_side][this_sec] = {this_sb_name: {"curr": [],
                                                                                 "avg": [],
                                                                                 "hpos": 0,
                                                                                 "vpos": 0}}
                else:
                    this_sb_dict[this_rb][this_side][this_sec][this_sb_name] = {"curr": [],
                                                                                "avg": [],
                                                                                "hpos": 0,
                                                                                "vpos": 0}

                this_sfd_webid = [x["WebId"] for x in this_atts if x["Name"].lower() == "sfd_priority_filtered"][0]
                this_sfd_srch_str = "streams/" + this_sfd_webid + "/recorded?StartTime=" + start_date + "&EndTime=" \
                                    + end_date + "&boundarytype=Outside"
                this_sfd_srch = requests.get(piwebapi_base_srch + this_sfd_srch_str,
                                             verify=False,
                                             auth=piwebapi_user).json()["Items"]
                this_sfd_avg_str = "streams/" + this_sfd_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                                   + end_date + "&SummaryType=Average"
                this_sfd_avg = requests.get(piwebapi_base_srch + this_sfd_avg_str,
                                            verify=False,
                                            auth=piwebapi_user).json()["Items"]
                if len(this_sfd_avg) > 0:
                    this_sfd_avg_val = this_sfd_avg[0]["Value"]
                else:
                    this_sfd_avg_val = "NA"

                this_inds = [this_i for this_i, x in enumerate(this_sfd_srch) if type(x["Value"]) == int]
                this_sb_dict[this_rb][this_side][this_sec][this_sb_name]["curr"] = [this_sfd_srch[x]["Value"]
                                                                                    for x in this_inds]
                this_sb_dict[this_rb][this_side][this_sec][this_sb_name]["tds"] = [this_sfd_srch[x]["Timestamp"]
                                                                                   for x in this_inds]
                this_sb_dict[this_rb][this_side][this_sec][this_sb_name]["curr"] = [x["Value"] for x in this_sfd_srch if
                                                                                    type(x["Value"]) == int]
                this_sb_dict[this_rb][this_side][this_sec][this_sb_name]["avg"] = this_sfd_avg_val
                this_hpos = this_hpos_srch[0]["Value"]
                this_vpos = this_vpos_srch[0]["Value"]
                this_h_max = max(int(this_hpos), this_h_max)
                this_v_max = max(int(this_vpos), this_v_max)
                this_sb_dict[this_rb][this_side][this_sec][this_sb_name]["hpos"] = this_hpos_srch[0]["Value"]
                this_sb_dict[this_rb][this_side][this_sec][this_sb_name]["vpos"] = this_vpos_srch[0]["Value"]

            this_sb_dict[this_rb]["max_v"] = this_v_max
            this_sb_dict[this_rb]["max_h"] = this_h_max

        else:
            this_sb_dict[this_rb] = {}

    return this_sb_dict


def get_zce_dict(this_site, this_z_config, this_rb, start_date, end_date):
    """Getting removal data from ZCE element"""
    this_db = site_db[this_site]
    this_zce_dict = {}
    this_z_srch_str = r"search/query?q=name:ZCE&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site
    this_z_srch = requests.get(piwebapi_base_srch + this_z_srch_str, auth=piwebapi_user, verify=False).json()['Items']
    if this_site not in ["Riegelwood"]:
        for this_zce in this_z_srch:
            this_rbname_srch = requests.get(this_zce["Links"]["Self"],
                                            verify=False,
                                            auth=piwebapi_user).json()
            this_rb_name = this_rbname_srch["Path"].split("\\")[5]
            print("Working on ZCE for " + this_site + " " + this_rb)
            if this_rb == this_rb_name:
                this_zce_dict[this_rb] = {}
                this_znames_webid = [x["WebId"] for x in this_zce["Attributes"] if x["Name"] == "ZoneName"][0]
                this_starts_webid = [x["WebId"] for x in this_zce["Attributes"] if x["Name"] == "StartTime"][0]
                this_remret_webid = [x["WebId"] for x in this_zce["Attributes"] if x["Name"] == "RemovalRetract"][0]
                this_remins_webid = [x["WebId"] for x in this_zce["Attributes"] if x["Name"] == "RemovalInsert"][0]
                this_sbs_webid = [x["WebId"] for x in this_zce["Attributes"] if x["Name"] == "SootblowerName"][0]
                this_znames_srch_str = "streams/" + this_znames_webid + "/recorded?StartTime=" + start_date + \
                                       "&EndTime=" + end_date + "&maxcount=100000"
                this_starts_srch_str = "streams/" + this_starts_webid + "/recorded?StartTime=" + start_date + \
                                       "&EndTime=" + end_date + "&maxcount=100000"
                this_remret_srch_str = "streams/" + this_remret_webid + "/recorded?StartTime=" + start_date + \
                                       "&EndTime=" + end_date + "&maxcount=100000"
                this_remins_srch_str = "streams/" + this_remins_webid + "/recorded?StartTime=" + start_date + \
                                       "&EndTime=" + end_date + "&maxcount=100000"
                this_sbs_srch_str = "streams/" + this_sbs_webid + "/recorded?StartTime=" + start_date + \
                                    "&EndTime=" + end_date + "&maxcount=100000"
                this_znames = requests.get(piwebapi_base_srch + this_znames_srch_str,
                                           auth=piwebapi_user,
                                           verify=False).json()["Items"]
                this_starts = requests.get(piwebapi_base_srch + this_starts_srch_str,
                                           auth=piwebapi_user,
                                           verify=False).json()["Items"]
                this_remret = requests.get(piwebapi_base_srch + this_remret_srch_str,
                                           auth=piwebapi_user,
                                           verify=False).json()["Items"]
                this_remins = requests.get(piwebapi_base_srch + this_remins_srch_str,
                                           auth=piwebapi_user,
                                           verify=False).json()["Items"]
                this_sbs = requests.get(piwebapi_base_srch + this_sbs_srch_str,
                                        auth=piwebapi_user,
                                        verify=False).json()["Items"]

                this_rr_dict = {}
                for this_ind in range(len(this_remret)):
                    this_rr_dict[this_remret[this_ind]["Timestamp"]] = this_remret[this_ind]["Value"]
                this_ri_dict = {}
                for this_ind in range(len(this_remins)):
                    this_ri_dict[this_remins[this_ind]["Timestamp"]] = this_remins[this_ind]["Value"]

                this_td_list = [x["Timestamp"] for x in this_znames]
                this_dict = {"z": [],
                             "rr": [],
                             "ri": [],
                             "sb": [],
                             "tds": []}

                for this_ind in range(len(this_td_list)):
                    try:
                        this_rr = this_rr_dict[this_td_list[this_ind]]
                    except:
                        this_rr = 0
                    try:
                        this_ri = this_ri_dict[this_td_list[this_ind]]
                    except:
                        this_ri = 0
                    this_dict["z"].append(this_znames[this_ind]["Value"])
                    this_dict["rr"].append(this_rr)
                    this_dict["ri"].append(this_ri)
                    this_dict["sb"].append(this_sbs[this_ind]["Value"])
                    if this_site == "Springfield":
                        this_dict["tds"].append(this_starts[this_ind]["Timestamp"])
                    else:
                        this_dict["tds"].append(this_starts[this_ind]["Value"])
                try:
                    this_dict["z"] = [this_z_config[this_rb][int(x.strip())]["Name"] for x in this_dict["z"]]
                except:
                    this_dict["z"] = [x.strip() for x in this_dict["z"]]
                try:
                    this_dict["sb"] = ["SB{:02n}".format(int(sb)) for sb in this_dict["sb"]]
                except:
                    this_dict["sb"] = [x.strip() for x in this_dict["sb"]]

                for this_ind in range(0, len(this_dict["sb"])):
                    if len(this_dict["sb"][this_ind]) == 3:
                        this_dict["sb"][this_ind] = this_dict["sb"][this_ind].replace(this_dict["sb"][this_ind][-1],
                                                                                      "0" + this_dict["sb"][this_ind][-1])

                this_zones = list(set(this_dict["z"]))
                if len([x for x in this_zones if len(x) < 4]) > 0:
                    for this_ind in range(0, len(this_zones)):
                        if len(this_zones[this_ind]) == 3:
                            this_zones[this_ind] = this_zones[this_ind][:2] + str(0) + this_zones[this_ind][-1]
                try:
                    this_zones = [x for x in this_zones if int(x) > 0]
                except:
                    this_zones = [x for x in this_zones if int(x[-2:]) > 0]
                this_lower_zones = [x.lower() for x in this_zones]
                this_wt_zones = len([x for x in this_zones if "wt" in x.lower()])
                for this_zone in sorted(this_zones):
                    this_z_int = int(this_zone[-2:])
                    if "dp" in this_zone.lower() and ("wt" + this_zone[-2:]) in this_lower_zones:
                        this_z_int += this_wt_zones

                    this_z_inds = [i for i, x in enumerate(this_dict["z"]) if
                                   this_dict["sb"][i] in this_z_config[this_rb][this_z_int]["sbs"]]

                    if len(this_z_inds) > 0:
                        this_zce_dict[this_rb][this_zone] = {"sbs": [this_dict["sb"][ind] for ind in this_z_inds],
                                                             "rins": [this_dict["ri"][ind] for ind in this_z_inds],
                                                             "rret": [this_dict["rr"][ind] for ind in this_z_inds],
                                                             "tds": [this_dict["tds"][ind] for ind in this_z_inds]}
    else:
        this_zce_dict = "No ZCE Data"

    return this_zce_dict


def get_perf_dict(this_site, this_rb, start_date, end_date):
    """Gets performance data for attributes on the RB element with 'Performance' category"""
    this_db = site_db[this_site]
    this_rb_srch_str = r"search/query?q=name:RB*&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site
    this_rb_ele = requests.get(piwebapi_base_srch + this_rb_srch_str,
                               verify=False,
                               auth=piwebapi_user).json()
    this_perf_dict = {}

    for this_ele in this_rb_ele["Items"]:
        this_rb_name = this_ele["Name"]
        if this_rb == this_rb_name:
            if "Attributes" in list(this_ele.keys()):
                print("Gathering performance data for " + this_site + " " + this_rb_name)
                this_perf_dict[this_rb_name] = {}
                this_perf_atts = [x for x in this_ele["Attributes"] if "Performance" in [c["Name"] for c in x["Categories"]]]
                for this_att in this_perf_atts:
                    this_att_webid = this_att["WebId"]
                    try:
                        this_att_uom = this_att["UoM"]
                    except:
                        this_att_uom = ""
                    this_att_srch_str = "streams/" + this_att_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                                        + end_date + "&summaryDuration=1h&SummaryType=Average"
                    this_att_srch = requests.get(piwebapi_base_srch + this_att_srch_str,
                                                 verify=False,
                                                 auth=piwebapi_user).json()["Items"]
                    this_vals = [x["Value"]["Value"] for x in this_att_srch]
                    if len([x for x in this_vals if x != None]) > 0:
                        this_perf_dict[this_rb_name][this_att["Name"]] = {
                            "vals": [x["Value"]["Value"] for x in this_att_srch],
                            "tds": [parse(x["Value"]["Timestamp"]) for x in this_att_srch],
                            "uom": this_att_uom}
    return this_perf_dict


def get_stm_ops_plot(this_site, this_ops_dict, start_date, end_date):
    fig_dict = {}
    this_db = site_db[this_site]
    rb_srch_str = r"search/query?q=name:RB*&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site + "\\"
    this_rb_ele = requests.get(piwebapi_base_srch + rb_srch_str,
                               verify=False,
                               auth=piwebapi_user).json()["Items"]
    this_fig_dict = {}
    for this_rb in this_rb_ele:
        plt.rcParams['axes.facecolor'] = '#cdd0db'
        fig, ax = plt.subplots(2, 1, sharex="all")
        fig.patch.set_facecolor("#cdd0db")
        this_rb_name = this_rb["Name"]
        this_fig_dict[this_rb_name] = []
        print("Generating Steam/Ops plot for " + this_site + " " + this_rb_name)
        this_rb_atts = this_rb["Attributes"]
        this_mcr_att = [x for x in this_rb_atts if x["Name"] == "Sootblowing_Steam_MCR"][0]
        this_mcr_webid = this_mcr_att["WebId"]
        this_tar_webid = [x["WebId"] for x in this_mcr_att["Attributes"] if x["Name"] == "Target"][0]
        this_bl_webid = [x["WebId"] for x in this_mcr_att["Attributes"] if x["Name"] == "Baseline"][0]
        this_dly_mcr_str = "streams/" + this_mcr_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                           + end_date + "&summaryDuration=1d&SummaryType=Average"
        this_hrly_mcr_str = "streams/" + this_mcr_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                            + end_date + "&summaryDuration=1h&SummaryType=Average"
        this_dly_mcr = requests.get(piwebapi_base_srch + this_dly_mcr_str,
                                    verify=False,
                                    auth=piwebapi_user).json()["Items"]
        this_hrly_mcr = requests.get(piwebapi_base_srch + this_hrly_mcr_str,
                                     verify=False,
                                     auth=piwebapi_user).json()["Items"]
        this_tar_str = "attributes/" + this_tar_webid
        this_tar_val = requests.get(piwebapi_base_srch + "streams/" + this_tar_webid + "/recorded?StartTime=01/01/1970",
                                    auth=piwebapi_user,
                                    verify=False).json()["Items"][0]["Value"]
        this_bl_val = requests.get(piwebapi_base_srch + "streams/" + this_bl_webid + "/recorded?StartTime=01/01/1970",
                                   auth=piwebapi_user,
                                   verify=False).json()["Items"][0]["Value"]

        this_dly_stm_mcr_y = [x["Value"]["Value"] for x in this_dly_mcr]
        this_hrly_stm_mcr_y = [x["Value"]["Value"] for x in this_hrly_mcr]
        if len([x for x in this_hrly_stm_mcr_y if type(x) != float]) > 0:
            this_rep_ind = [i for i, x in enumerate(this_hrly_stm_mcr_y) if type(x) != float]
            for this_ind in this_rep_ind:
                this_hrly_stm_mcr_y[this_ind] = 0
        this_dly_stm_mcr_x = [parse(x["Value"]["Timestamp"]) for x in this_dly_mcr]
        this_hrly_stm_mcr_x = [parse(x["Value"]["Timestamp"]) for x in
                               this_hrly_mcr]

        fig.set_size_inches(11.5, 6)
        fig.set_tight_layout(True)
        locator = mdates.DayLocator()
        formatter = mdates.DateFormatter("%m/%d")
        ax[0].xaxis.set_major_locator(locator)
        ax[0].xaxis.set_major_formatter(formatter)
        ax[0].xaxis.set_minor_locator(mdates.DayLocator())
        ax[0].plot(this_dly_stm_mcr_x, this_dly_stm_mcr_y, c="#12284B", label="Daily")
        ax[0].plot(this_hrly_stm_mcr_x, this_hrly_stm_mcr_y, c="#0099D6", alpha=0.25, label="Hourly")
        if this_site != "FlintRiver":
            ax[0].plot(this_hrly_stm_mcr_x, [this_tar_val] * len(this_hrly_stm_mcr_x), '--', c="#00B189",
                       label="Target")
            ax[0].plot(this_hrly_stm_mcr_x, [this_bl_val] * len(this_hrly_stm_mcr_x), '--', c="#FAA216",
                       label="Baseline")
        ax[0].set_title("Steam Usage")
        mcr_c = c_config["CIRe"]
        if this_site not in ["Lewiston", "FlintRiver"]:
            if this_dly_stm_mcr_y[-1] <= this_tar_val:
                mcr_c = c_config["CIGr"]
            elif this_dly_stm_mcr_y[-1] <= this_bl_val:
                mcr_c = c_config["CIYe"]
        this_stm_avg = np.average(this_hrly_stm_mcr_y)
        ax[0].plot(this_hrly_stm_mcr_x, [this_stm_avg] * len(this_hrly_stm_mcr_x),
                   c=c_config["CINB"],
                   linestyle="dotted",
                   alpha=0.6,
                   label="Month Avg")
        ax[0].plot(this_hrly_stm_mcr_x[-1],
                   this_stm_avg,
                   marker='x',
                   c=mcr_c,
                   markersize=8,
                   mew=4)
        ax[0].annotate(round(this_stm_avg, 2), [this_hrly_stm_mcr_x[-1], this_stm_avg + 0.15],
                       size=14,
                       horizontalalignment='center',
                       c=mcr_c)
        box = ax[0].get_position()
        ax[0].set_position([box.x0, box.y0, box.width * 0.85, box.height])
        ax[0].legend(loc='center left',
                     bbox_to_anchor=(1, 0.5),
                     fontsize='small',
                     labelspacing=0)
        ax[0].spines["right"].set_visible(False)
        ax[0].spines["top"].set_visible(False)
        ax[0].set_ylabel("MCR (%)")

        this_days = list(this_ops_dict[this_rb_name].keys())
        this_zone = [this_ops_dict[this_rb_name][x]["Zone"] for x in this_days]
        this_timer = [this_ops_dict[this_rb_name][x]["Timer"] for x in this_days]
        this_rule = [this_ops_dict[this_rb_name][x]["Rule"] for x in this_days]
        this_man = [this_ops_dict[this_rb_name][x]["Man"] for x in this_days]
        this_ow = [this_ops_dict[this_rb_name][x]["OneWay"] for x in this_days]

        this_t_bot = this_zone
        this_r_bot = np.add(this_timer, this_t_bot)
        this_m_bot = np.add(this_rule, this_r_bot)
        this_days_x = [dt.datetime.combine(x, dt.datetime.min.time()) for x in this_days]

        ax[1].bar(height=this_zone, x=this_days_x, label="Zone", color=c_config["CIGr"])
        ax[1].bar(height=this_timer, x=this_days_x, bottom=this_t_bot, label="Timer", color=c_config["CICy"])
        ax[1].bar(height=this_rule, x=this_days_x, bottom=this_r_bot, label="Rule", color=c_config["CIRe"])
        ax[1].bar(height=this_man, x=this_days_x, bottom=this_m_bot, label="Manual", color=c_config["CIYe"])
        this_width = ax[1].patches[0].get_window_extent(fig.canvas.get_renderer()).width
        formatter = mdates.DateFormatter("%m/%d")
        ax[1].xaxis.set_major_locator(locator)
        ax[1].xaxis.set_major_formatter(formatter)
        ax[1].xaxis.set_minor_locator(mdates.DayLocator())
        ax[1].plot(this_days_x, this_ow,
                   marker="_",
                   color=c_config["CINB"],
                   zorder=100,
                   label="One-Way",
                   linewidth=0,
                   markersize=this_width * 0.8,
                   mew=4)
        ax[1].tick_params(labelrotation=45)
        ax[1].set_title("Daily Operations")
        box = ax[1].get_position()
        ax[1].set_position([box.x0, box.y0, box.width * 0.85, box.height])
        handles, labels = ax[1].get_legend_handles_labels()
        handles[0].set_markersize(16)
        handles = reversed(handles)
        labels = reversed(labels)
        ax[1].legend(handles, labels,
                     loc='center left',
                     bbox_to_anchor=(1, 0.5),
                     fontsize="small",
                     labelspacing=0)
        ax[1].spines["top"].set_visible(False)
        ax[1].spines["right"].set_visible(False)
        ax[1].set_ylabel("Operations")
        buf = io.BytesIO()
        fig.savefig(buf, format="png", dpi=300, bbox_inches='tight')
        buf.seek(0)
        plt_return = base64.b64encode(buf.getvalue()).decode("utf-8")
        fig_dict[''] = plt_return
    return fig_dict


def get_perf_plots(this_perf_dict, this_site, this_rb):
    """Plots data performance attributes with black liquor"""
    this_fig_dict = {}
    for this_rb in this_perf_dict:
        print("Generating performance plots for " + this_site + " " + this_rb)
        this_bl_name = [x for x in this_perf_dict[this_rb] if "liquor" in x.lower()][0]
        this_bl = this_perf_dict[this_rb][this_bl_name]
        this_bl_y = this_bl["vals"]
        this_bl_x = this_bl["tds"]
        this_p_list = list(this_perf_dict[this_rb].keys())
        this_p_list.remove(this_bl_name)
        this_perf_len = len(this_p_list)
        this_p_num = 0
        while this_p_num < this_perf_len:
            this_plot_list = ''
            this_rem_plots = this_perf_len - this_p_num
            this_p_rows = min(3, this_rem_plots)
            this_fig, this_ax = plt.subplots(nrows=this_p_rows, ncols=1)
            this_fig.patch.set_facecolor("#cdd0db")
            this_fig.set_size_inches(11.5, 6)
            this_fig.set_tight_layout(True)
            locator = mdates.DayLocator()
            if this_p_rows > 1:
                this_bot_axis = this_ax[-1]
            else:
                this_bot_axis = this_ax
            for this_p in range(0, this_p_rows):
                try:
                    this_p_axis = this_ax[this_p]
                except:
                    this_p_axis = this_ax
                this_label = this_p_list[this_p_num]
                this_label = this_label.replace("_", " ")
                this_ylab = "".join([x[0] for x in this_label.split()])
                if len(this_perf_dict[this_rb][this_p_list[this_p_num]]["uom"]) > 0:
                    this_ylab = this_ylab + " (" + this_perf_dict[this_rb][this_p_list[this_p_num]]["uom"] + ")"
                this_p_axis.plot(this_perf_dict[this_rb][this_p_list[this_p_num]]["tds"],
                                 this_perf_dict[this_rb][this_p_list[this_p_num]]["vals"],
                                 c=c_config["CINB"],
                                 label=this_label)

                this_max = max(this_perf_dict[this_rb][this_p_list[this_p_num]]["vals"])
                this_min = min(this_perf_dict[this_rb][this_p_list[this_p_num]]["vals"])
                this_y_min = this_min - (this_max * 0.25)
                this_y_max = this_max * 1.25
                this_p_axis.set_ylim(this_y_min, this_y_max)
                this_p_axis.set_ylabel(this_ylab)
                this_p_axis.set_title(this_label)
                this_ax_01 = this_p_axis.twinx()
                this_ax_01.plot(this_bl_x, this_bl_y, c=c_config["CICy"], alpha=0.5, label="Black Liquor Flow")
                this_max = max(this_bl_y)
                this_min = min(this_bl_y)
                this_y_min = this_min - (this_max * 0.25)
                this_y_max = this_max * 1.25
                this_ax_01.set_ylim(this_y_min, this_y_max)
                this_ax_01.set_ylabel("BL Flow (GPM)")
                handles0, labels0 = this_p_axis.get_legend_handles_labels()
                handles1, labels1 = this_ax_01.get_legend_handles_labels()
                this_p_num += 1
                formatter = mdates.DateFormatter("%m/%d")
                this_p_axis.xaxis.set_major_locator(locator)
                this_p_axis.xaxis.set_major_formatter(formatter)
                this_p_axis.xaxis.set_minor_locator(mdates.DayLocator())
                this_p_axis.legend([handles0[0], handles1[0]],
                                   [labels0[0], labels1[0]],
                                   loc='lower left',
                                   labelspacing=0,
                                   ncol=1,
                                   fontsize='small')
                this_p_axis.tick_params(labelrotation=45)
                this_p_axis.spines["top"].set_visible(False)
                this_ax_01.spines["top"].set_visible(False)
                this_plot_list += '\n' + this_label
            buf = io.BytesIO()
            this_fig.savefig(buf, format="png", dpi=300, bbox_inches='tight')
            buf.seek(0)
            plt_return = base64.b64encode(buf.getvalue()).decode("utf-8")
            this_fig_dict[this_plot_list] = plt_return
    return this_fig_dict


def get_zones_plots(this_zce_dict, this_zone_dict, this_z_config, start_date, end_date):
    """Generates yearly zone plot, hourly PV/ACC vs setpoints, stacked removals"""
    this_fig_dict = {}
    for this_rb in this_zone_dict:
        this_z_list = sorted(list(set(this_zone_dict[this_rb].keys())))
        this_zce_list = sorted(list(set(this_zce_dict[this_rb].keys())))
        try:
            this_zce_name_list = [this_z_config[this_rb][int(x)]["Name"] for x in this_zce_list]
        except:
            this_zce_name_list = this_zce_list
        for this_z in this_z_list:
            print("Generating zone plots for " + this_rb + " " + this_z)
            last_date = parse(start_date).replace(tzinfo=dt.timezone.utc)
            this_pv_inds = [ind for ind, x in enumerate(this_zone_dict[this_rb][this_z]["PV"]["tds"]) if x > last_date]
            this_pv_y = [this_zone_dict[this_rb][this_z]["PV"]["vals"][ind] for ind in this_pv_inds]
            this_pv_x = [this_zone_dict[this_rb][this_z]["PV"]["tds"][ind] for ind in this_pv_inds]
            if "ACC" in list(this_zone_dict[this_rb][this_z].keys()):
                this_acc_inds = [ind for ind, x in enumerate(this_zone_dict[this_rb][this_z]["ACC"]["tds"]) if
                                 x > last_date]
                this_nbr_inds = [ind for ind, x in enumerate(this_zone_dict[this_rb][this_z]["NBR"]["tds"]) if
                                 x > last_date]
                this_hpv_y = [this_zone_dict[this_rb][this_z]["ACC"]["vals"][ind] for ind in this_acc_inds]
                this_hpv_x = [this_zone_dict[this_rb][this_z]["ACC"]["tds"][ind] for ind in this_acc_inds]
                this_sp1_y = [this_zone_dict[this_rb][this_z]["NBR"]["vals"][ind] for ind in this_nbr_inds]
                this_sp1_x = [this_zone_dict[this_rb][this_z]["NBR"]["tds"][ind] for ind in this_nbr_inds]
                this_p2_ys = [this_hpv_y, this_sp1_y]
                this_p2_xs = [this_hpv_x, this_sp1_x]
                this_labels = ["ACC", "NBR"]
                this_c_list = [c_config["CINB"], c_config["CIYe"]]
                this_ls_list = ["solid", "dashed", "dashed"]

            else:
                this_sp1_inds = [ind for ind, x in enumerate(this_zone_dict[this_rb][this_z]["Clean_SP"]["tds"]) if
                                 x > last_date]
                this_sp2_inds = [ind for ind, x in enumerate(this_zone_dict[this_rb][this_z]["Dirty_SP"]["tds"]) if
                                 x > last_date]
                this_csp_y = [this_zone_dict[this_rb][this_z]["Clean_SP"]["vals"][ind] for ind in this_sp1_inds]
                this_csp_x = [this_zone_dict[this_rb][this_z]["Clean_SP"]["tds"][ind] for ind in this_sp1_inds]
                this_dsp_y = [this_zone_dict[this_rb][this_z]["Dirty_SP"]["vals"][ind] for ind in this_sp2_inds]
                this_dsp_x = [this_zone_dict[this_rb][this_z]["Dirty_SP"]["tds"][ind] for ind in this_sp2_inds]
                this_p2_ys = [this_pv_y, this_csp_y, this_dsp_y]
                this_p2_xs = [this_pv_x, this_csp_x, this_dsp_x]
                this_labels = ["PV", "Clean SP", "Dirty SP"]
                this_c_list = [c_config["CINB"], c_config["CIGr"], c_config["CIYe"]]
                this_ls_list = ["solid", "dashed", "dashed"]

            this_dly_avg = []
            this_ds = sorted(list(set([x.date() for x in this_pv_x])))
            for this_ind in range(len(this_pv_y)):
                if type(this_pv_y[this_ind]) != float:
                    this_pv_y[this_ind] = 0
            for this_d in this_ds:
                this_dly_avg.append(
                    np.average([this_pv_y[ind] for ind, x in enumerate(this_pv_x) if x.date() == this_d]))

            fig = plt.figure(constrained_layout=True)
            ax = []
            spec2 = gridspec.GridSpec(ncols=1, nrows=100, figure=fig)
            ax.append(fig.add_subplot(spec2[0:18, :]))
            ax.append(fig.add_subplot(spec2[23:53, :]))
            ax.append(fig.add_subplot(spec2[55:95, :]))
            ax[1].xaxis.set_ticklabels([])
            fig.patch.set_facecolor("#cdd0db")
            fig.set_size_inches(13, 7)
            fig.set_tight_layout(True)
            fig.suptitle(this_z,
                         size='x-large')
            locator = mdates.DayLocator()
            formatter = mdates.DateFormatter("%m/%d")
            ax[2].xaxis.set_major_locator(locator)
            ax[2].xaxis.set_major_formatter(formatter)
            ax[2].xaxis.set_minor_locator(mdates.DayLocator())
            ax[2].tick_params(labelrotation=45)

            #           plot pv yearly by day, +/- 2 stdev, min/max
            this_yrly_ds = sorted(list(set([x.date() for x in this_zone_dict[this_rb][this_z]["YR_PV"]["tds"]])))
            this_yrly_ys = this_zone_dict[this_rb][this_z]["YR_PV"]["vals"]
            this_yrly_filt_inds = [this_i for this_i, x in enumerate(this_yrly_ys) if type(x) == float]
            this_yrly_ds = [this_yrly_ds[ind] for ind in this_yrly_filt_inds]
            this_yrly_ys = [this_yrly_ys[ind] for ind in this_yrly_filt_inds]
            this_yr_avg = np.average(this_yrly_ys)
            this_yr_sd = np.std(this_yrly_ys)
            ax[0].xaxis.set_major_locator(mdates.MonthLocator())
            ax[0].xaxis.set_major_formatter(mdates.DateFormatter("%m/%y"))
            ax[2].xaxis.set_minor_locator(mdates.DayLocator())
            ax[0].plot(this_yrly_ds, this_yrly_ys,
                       c=c_config["CINB"],
                       label="Daily Avg.")
            ax[0].plot(this_yrly_ds, [this_yr_avg + 2 * this_yr_sd] * len(this_yrly_ds),
                       c=c_config["CIYe"],
                       label="UCL",
                       linestyle="dashed")
            ax[0].plot(this_yrly_ds, [this_yr_avg - 2 * this_yr_sd] * len(this_yrly_ds),
                       c=c_config["CIYe"],
                       label="LCL",
                       linestyle="dotted")
            ax[0].spines["top"].set_visible(False)
            ax[0].spines["right"].set_visible(False)
            this_change = max((max(this_yrly_ys) * 0.2, (this_yr_avg + 2 * this_yr_sd) * 0.2))
            this_y_max = max(max(this_yrly_ys) + this_change, (this_yr_avg + 2 * this_yr_sd) + this_change)
            this_y_min = min(min(this_yrly_ys) - this_change, (this_yr_avg - 2 * this_yr_sd) - this_change)
            ax[0].set_ylim(this_y_min, this_y_max)
            ax[0].set_ylabel("PV")
            ax[0].legend(fontsize='small',
                         loc='upper left',
                         labelspacing=0,
                         ncol=3)

            #           plot 2 is hourly zone values and setpoints
            for this_ind in range(len(this_p2_ys)):
                ax[1].plot(this_p2_xs[this_ind], this_p2_ys[this_ind],
                           c=this_c_list[this_ind],
                           label=this_labels[this_ind],
                           linestyle=this_ls_list[this_ind])
                ax[1].legend(loc=2,
                             ncol=3,
                             fontsize='small')
                ax[1].set_xticklabels([])
                ax[1].spines["top"].set_visible(False)
                ax[1].spines["right"].set_visible(False)

            #           plt3 is removals stacked
            if this_z in this_zce_list:
                this_sb_list = list(set(this_zce_dict[this_rb][this_z]['sbs']))
                this_rem_inss = this_zce_dict[this_rb][this_z]["rins"]
                this_rem_rets = this_zce_dict[this_rb][this_z]["rret"]
                this_sbs_list = this_zce_dict[this_rb][this_z]["sbs"]
                this_tds_list = [parse(x) for x in
                                 this_zce_dict[this_rb][this_z]["tds"]]

                this_sb_dict = {}
                for this_sb in this_sb_list:
                    this_sb_ind = [ind for ind, x in enumerate(this_sbs_list) if x == this_sb]
                    this_sb_rem_ins = [this_rem_inss[ind] for ind in this_sb_ind]
                    this_sb_rem_ret = [this_rem_rets[ind] for ind in this_sb_ind]
                    this_sb_tds = [this_tds_list[ind] for ind in this_sb_ind]

                    this_sb_dly_ins_avg = []
                    this_sb_dly_ret_avg = []
                    for this_d in this_ds:
                        this_sb_dly_ins_avg.append(
                            np.average(
                                [this_sb_rem_ins[ind] for ind, x in enumerate(this_sb_tds) if x.date() == this_d]))
                        this_sb_dly_ret_avg.append(
                            np.average(
                                [this_sb_rem_ret[ind] for ind, x in enumerate(this_sb_tds) if x.date() == this_d]))
                    this_sb_dict[this_sb] = {"ins": this_sb_dly_ins_avg,
                                             "ret": this_sb_dly_ret_avg,
                                             "tot": np.add(this_sb_dly_ins_avg, this_sb_dly_ret_avg)}

                ax[2].set_prop_cycle(custom_cycler)
                this_labels = list(this_sb_dict.keys())
                if len(this_labels) > len(custom_cycler._left):
                    this_c_cycler = list([x['color'] for x in custom_cycler._left])
                    [this_c_cycler.append(custom_cycler._left[x]['color']) for x in
                     range(len(this_labels) - len(custom_cycler._left))]
                    this_c_list = this_c_cycler
                else:
                    this_c_list = [custom_cycler._left[x]['color'] for x in range(len(this_labels))]
                this_leg_list = []
                for i in range(len(this_labels)):
                    this_leg_list.append(mpatches.Patch(color=this_c_list[i], label=this_labels[i]))
                ax[2].stackplot(np.array(this_ds), [abs(this_sb_dict[x]["tot"]) for x in this_sb_dict])
                ax[2].legend(handles=this_leg_list,
                             loc='upper left',
                             ncol=len(this_leg_list),
                             fontsize='small')
                ax[2].spines["top"].set_visible(False)
                ax[2].spines["right"].set_visible(False)

            elif this_z in this_zce_name_list:
                this_z_n = str(int(this_z[-2:]))
                this_sb_list = list(set(this_zce_dict[this_rb][this_z_n]['sbs']))
                this_rem_inss = this_zce_dict[this_rb][this_z_n]["rins"]
                this_rem_rets = this_zce_dict[this_rb][this_z_n]["rret"]
                this_sbs_list = this_zce_dict[this_rb][this_z_n]["sbs"]
                this_tds_list = [parse(x) for x in
                                 this_zce_dict[this_rb][this_z_n]["tds"]]

                this_sb_dict = {}
                for this_sb in this_sb_list:
                    this_sb_ind = [ind for ind, x in enumerate(this_sbs_list) if x == this_sb]
                    this_sb_rem_ins = [this_rem_inss[ind] for ind in this_sb_ind]
                    this_sb_rem_ret = [this_rem_rets[ind] for ind in this_sb_ind]
                    this_sb_tds = [this_tds_list[ind] for ind in this_sb_ind]

                    this_sb_dly_ins_avg = []
                    this_sb_dly_ret_avg = []
                    for this_d in this_ds:
                        this_sb_dly_ins_avg.append(
                            np.average(
                                [this_sb_rem_ins[ind] for ind, x in enumerate(this_sb_tds) if x.date() == this_d]))
                        this_sb_dly_ret_avg.append(
                            np.average(
                                [this_sb_rem_ret[ind] for ind, x in enumerate(this_sb_tds) if x.date() == this_d]))
                        this_sb_dict[this_sb] = {"ins": this_sb_dly_ins_avg,
                                                 "ret": this_sb_dly_ret_avg,
                                                 "tot": np.add(this_sb_dly_ins_avg, this_sb_dly_ret_avg)}

                ax[2].set_prop_cycle(custom_cycler)
                this_labels = list(this_sb_dict.keys())
                if len(this_labels) > len(custom_cycler._left):
                    this_c_cycler = list([x['color'] for x in custom_cycler._left])
                    [this_c_cycler.append(custom_cycler._left[x]['color']) for x in
                     range(len(this_labels) - len(custom_cycler._left))]
                    this_c_list = this_c_cycler
                else:
                    this_c_list = [custom_cycler._left[x]['color'] for x in range(len(this_labels))]
                this_leg_list = []
                for i in range(len(this_labels)):
                    this_leg_list.append(mpatches.Patch(color=this_c_list[i], label=this_labels[i]))
                ax[2].stackplot(np.array(this_ds), [abs(this_sb_dict[x]["tot"]) for x in this_sb_dict])
                ax[2].legend(handles=this_leg_list,
                             ncol=len(this_leg_list),
                             fontsize='small',
                             loc="upper left")
                ax[2].spines["top"].set_visible(False)
                ax[2].spines["right"].set_visible(False)

            else:
                ax[2].plot(this_p2_xs[this_ind], [0] * len(this_p2_xs[this_ind]), alpha=0)
                ax[2].annotate("No Assigned Blowers", [min(this_ds) + dt.timedelta(days=1), 0],
                               size=14,
                               horizontalalignment='left',
                               verticalalignment='center',
                               c=c_config["CINB"])
            buf = io.BytesIO()
            fig.savefig(buf, format="png", dpi=300, bbox_inches='tight')
            buf.seek(0)
            plt_return = base64.b64encode(buf.getvalue()).decode("utf-8")
            this_fig_dict[this_z] = plt_return

    return this_fig_dict


def get_flow_plots(this_sb_dict, this_opt, this_site, this_rb):
    """Makes flow plots based on our calculated average"""
    this_fig_dict = {}
    for this_rb in this_sb_dict:
        for this_sec in this_sb_dict[this_rb]:
            print("Generating flow plots for " + this_site + " " + this_rb + " " + this_sec)
            this_plt_dict = {}
            for this_fcv in this_sb_dict[this_rb][this_sec]:
                this_plt_dict[this_fcv] = {}
                for this_sb in this_sb_dict[this_rb][this_sec][this_fcv]:
                    this_plt_dict[this_fcv][this_sb] = {'f': this_sb_dict[this_rb][this_sec][this_fcv][this_sb]['flow'],
                                                        'avg': np.average(this_sb_dict[this_rb][this_sec][this_fcv]
                                                                          [this_sb]["flow"]),
                                                        'sp': this_sb_dict[this_rb][this_sec][this_fcv][this_sb]["sp"]}
            if np.average(this_plt_dict[this_fcv][this_sb]['avg']) > 30000:
                this_plt_dict[this_fcv][this_sb]['avg'] = np.array(this_plt_dict[this_fcv][this_sb]['avg']) / 1000
            this_row_n = int(np.ceil(len(this_plt_dict) / 2))
            fig, ax = plt.subplots(nrows=this_row_n,
                                   ncols=2,
                                   constrained_layout=True)
            fig.patch.set_facecolor("#cdd0db")
            if len(this_plt_dict) < 2:
                ax[-1].axis("off")
            fig.suptitle(this_sec + " SB Flows",
                         ha='center',
                         va='top',
                         size=24)
            fig.set_size_inches(11.5, 6)
            fig.set_tight_layout(True)

            this_fcv_list = list(this_plt_dict.keys())
            this_max = max([len(this_plt_dict[x]) for x in this_plt_dict])
            this_count = 0
            this_col = 0
            this_row = 0
            for this_ind in range(0, len(this_fcv_list)):
                if ax.ndim > 1:
                    this_ax = ax[this_row, this_col]
                    this_row += 1
                    if this_row >= ax.shape[1]:
                        this_row = 0
                        this_col += 1
                else:
                    this_ax = ax[this_row + this_col]
                    this_col += 1
                this_m_s = this_ax.bbox.height / this_max * 0.55
                this_fcv = this_fcv_list[this_ind]
                this_sbs = sorted(list(this_plt_dict[this_fcv].keys()))
                this_avg_flows = [this_plt_dict[this_fcv][x]["avg"] for x in this_sbs]
                this_sps = [this_plt_dict[this_fcv][x]["sp"] for x in this_sbs]
                for i in range(0, len(this_avg_flows)):
                    if this_avg_flows[i] > 50000:
                        this_avg_flows[i] /= 1000
                for i in range(0, len(this_sps)):
                    if this_avg_flows[i] / this_sps[i] > 10:
                        this_sps[i] *= 1000
                this_ax.set_title(this_fcv)
                this_ax.barh(y=this_sbs,
                             width=this_avg_flows,
                             zorder=100,
                             alpha=0.33,
                             edgecolor=c_config["CICy"],
                             facecolor=c_config["CICy"])
                this_ax.plot(this_sps, this_sbs,
                             zorder=30,
                             c=c_config["CIYe"],
                             alpha=1,
                             marker="|",
                             linewidth=0,
                             markersize=this_m_s,
                             mew=3)
                this_ax.set_ylim(-1, this_max)
                this_avg_patch = mpatches.Patch(color=c_config["CICy"],
                                                label="Avg Flow",
                                                alpha=0.33)
                this_sp_patch = mpatches.Patch(color=c_config["CIYe"],
                                               label="Flow SP")

                if this_opt == "ops":
                    for this_sb in this_plt_dict[this_fcv]:
                        this_flows = this_plt_dict[this_fcv][this_sb]["f"]
                        if len(this_flows) > 0:
                            if this_flows[0] > 50000:
                                this_flows = np.array(this_flows) / 1000
                            this_ax.plot(this_flows,
                                         [this_sb] * len(this_flows),
                                         c=c_config["CINB"],
                                         alpha=1,
                                         marker="|",
                                         zorder=20,
                                         markersize=this_m_s,
                                         linewidth=0)
                            this_op_patch = mpatches.Patch(color=c_config["CINB"],
                                                           label="Op Flows")
                            this_ax.legend(labels=["Avg Flow", "Flow SP", "Op Flows"],
                                           handles=[this_avg_patch, this_sp_patch, this_op_patch],
                                           loc="upper right",
                                           fontsize="small")

                elif this_opt == "sd":
                    this_err = [np.std(this_plt_dict[this_fcv][sb]["f"]) for sb in this_plt_dict[this_fcv]]
                    if this_err[0] > this_avg_flows[0]:
                        this_err = np.array(this_err) / 1000
                    this_ax.errorbar(this_avg_flows,
                                     list(this_plt_dict[this_fcv].keys()),
                                     xerr=this_err,
                                     capsize=this_m_s / 2,
                                     fmt="none",
                                     ecolor=c_config["CINB"],
                                     capthick=2,
                                     elinewidth=2)
                    this_op_patch = mpatches.Patch(color=c_config["CINB"],
                                                   label="Std. Dev.")
                    this_ax.legend(labels=["Avg Flow", "Flow SP", "Std. Dev."],
                                   handles=[this_avg_patch, this_sp_patch, this_op_patch],
                                   loc="upper right",
                                   fontsize="small")

                if max(this_sps) > 100:
                    this_ax.set_xlim(0, 30000)
                    this_ax.set_xlim(0, 30000)
                else:
                    this_ax.set_xlim(0, 30)
                    this_ax.set_xlim(0, 30)
                this_count += 1

            buf = io.BytesIO()
            fig.savefig(buf, format="png", dpi=300, bbox_inches='tight')
            buf.seek(0)
            plt_return = base64.b64encode(buf.getvalue()).decode("utf-8")
            this_fig_dict[this_sec] = (plt_return)

    return this_fig_dict


def get_amp_plots(this_sb_dict, this_opt, this_site, this_rb):
    """Makes flow plots based on our calculated average"""
    this_fig_dict = {}
    for this_rb in this_sb_dict:
        for this_sec in this_sb_dict[this_rb]:
            print("Generating amp plots for " + this_site + " " + this_rb + " " + this_sec)
            this_plt_dict = {}
            for this_fcv in this_sb_dict[this_rb][this_sec]:
                this_plt_dict[this_fcv] = {}
                for this_sb in this_sb_dict[this_rb][this_sec][this_fcv]:
                    this_plt_dict[this_fcv][this_sb] = {
                        'ins': this_sb_dict[this_rb][this_sec][this_fcv][this_sb]['ins'],
                        'op': this_sb_dict[this_rb][this_sec][this_fcv][this_sb]['op'],
                        'i_avg': np.average(this_sb_dict[this_rb][this_sec][this_fcv]
                                            [this_sb]["ins"]),
                        'o_avg': np.average(this_sb_dict[this_rb][this_sec][this_fcv]
                                            [this_sb]["op"]),
                        }
            this_row_n = int(np.ceil(len(this_plt_dict) / 2))
            fig, ax = plt.subplots(nrows=this_row_n,
                                   ncols=2,
                                   constrained_layout=True)
            fig.suptitle(this_sec + " SB Amps",
                         ha='center',
                         va='top',
                         size=24)
            fig.patch.set_facecolor("#cdd0db")
            fig.set_size_inches(11.5, 6)
            fig.set_tight_layout(True)

            this_fcv_list = list(this_plt_dict.keys())
            this_max = max([len(this_plt_dict[x]) for x in this_plt_dict])
            this_count = 0
            this_col = 0
            this_row = 0
            for this_ind in range(0, len(this_fcv_list)):
                if ax.ndim > 1:
                    this_ax = ax[this_row, this_col]
                    this_row += 1
                    if this_row >= ax.shape[1]:
                        this_row = 0
                        this_col += 1
                else:
                    this_ax = ax[this_row + this_col]
                    this_col += 1
                this_m_s = this_ax.bbox.height / this_max * 0.55
                this_fcv = this_fcv_list[this_ind]
                this_sbs = sorted(list(this_plt_dict[this_fcv].keys()))
                this_insa = [this_plt_dict[this_fcv][x]["i_avg"] for x in this_sbs]
                this_opa = [this_plt_dict[this_fcv][x]["o_avg"] for x in this_sbs]
                this_ax.set_title(this_fcv)
                this_ax.barh(y=this_sbs,
                             width=this_insa,
                             zorder=100,
                             alpha=1,
                             edgecolor=c_config["CICy"],
                             facecolor=c_config["CICy"])
                this_opa_c = [c_config["CIYe"]] * len(this_opa)
                for this_ind in range(0, len(this_opa_c)):
                    if this_opa[this_ind] / this_insa[this_ind] > 1.5:
                        this_opa_c[this_ind] = c_config["CIRe"]
                this_ax.barh(y=this_sbs,
                             width=this_opa,
                             zorder=99,
                             alpha=0.33,
                             color=this_opa_c)
                this_ax.set_ylim(-1, this_max)
                this_ia_patch = mpatches.Patch(color=c_config["CICy"],
                                               label="Avg Flow",
                                               alpha=0.33)
                this_oa_patch = mpatches.Patch(color=c_config["CIYe"],
                                               label="Flow SP")

                if this_opt == "ops":
                    for this_sb in this_plt_dict[this_fcv]:
                        this_a = this_plt_dict[this_fcv][this_sb]["op"]
                        if len(this_a) > 0:
                            this_ax.plot(this_a,
                                         [this_sb] * len(this_a),
                                         c=c_config["CINB"],
                                         alpha=1,
                                         marker="|",
                                         zorder=20,
                                         markersize=this_m_s,
                                         linewidth=0)
                            this_op_patch = mpatches.Patch(color=c_config["CINB"],
                                                           label="Op Amps")
                            this_ax.legend(labels=["Ins Avg Amps", "Op Avg Amps", "Op Amps"],
                                           handles=[this_ia_patch, this_oa_patch, this_op_patch],
                                           loc="upper right",
                                           fontsize="small")

                else:
                    this_err = [np.std(this_plt_dict[this_fcv][sb]["op"]) for sb in this_plt_dict[this_fcv]]
                    this_ax.errorbar(this_opa,
                                     list(this_plt_dict[this_fcv].keys()),
                                     xerr=this_err,
                                     capsize=this_m_s / 2,
                                     fmt="none",
                                     ecolor=c_config["CINB"],
                                     capthick=2,
                                     elinewidth=2,
                                     zorder=101)
                    this_op_patch = mpatches.Patch(color=c_config["CINB"],
                                                   label="Std. Dev.")
                    this_ax.legend(labels=["Ins Avg Amps", "Op Avg Amps", "Op SD"],
                                   handles=[this_ia_patch, this_oa_patch, this_op_patch],
                                   loc="upper right",
                                   fontsize="small")

                this_ax.set_xlim(0, 6)
                this_ax.set_xlim(0, 6)
                this_count += 1

            buf = io.BytesIO()
            fig.savefig(buf, format="png", dpi=300, bbox_inches='tight')
            buf.seek(0)
            plt_return = base64.b64encode(buf.getvalue()).decode("utf-8")
            this_fig_dict[this_sec] = plt_return

    return this_fig_dict


def get_sfd_plot(this_sb_dict):
    """Makes flow plots based on our calculated average"""
    this_fig_dict = {}
    if len(this_sb_dict) > 0:
        this_cs = [c_config["CIGr"], c_config["CICy"], c_config["CINB"], c_config["CIYe"],
                   c_config["CIRe"], '#666666']
        for this_rb in this_sb_dict:
            print("Generating SFD plots for " + this_site + " " + this_rb)
            this_fig_dict[this_rb] = []
            this_plt_dict = {}
            this_side_list = [x for x in this_sb_dict[this_rb] if type(this_sb_dict[this_rb][x]) == dict]
            for this_side in this_side_list:
                this_plt_dict[this_side] = {}
                for this_sec in this_sb_dict[this_rb][this_side]:
                    this_plt_dict[this_side][this_sec] = {}
                    for this_sb in this_sb_dict[this_rb][this_side][this_sec]:

                        this_plt_dict[this_side][this_sec][this_sb] = {'pie': [],
                                                                       'avg': [this_sb_dict[this_rb][this_side]
                                                                               [this_sec][this_sb]['avg']['Value']][0],
                                                                       'hpos': [this_sb_dict[this_rb][this_side]
                                                                                [this_sec][this_sb]['hpos']][0],
                                                                       'vpos': [this_sb_dict[this_rb][this_side]
                                                                                [this_sec][this_sb]['vpos']][0]}
                        this_vals = this_sb_dict[this_rb][this_side][this_sec][this_sb]['curr']
                        this_len = len(this_vals)
                        this_start_date = this_sb_dict[this_rb][this_side][this_sec][this_sb]['tds'][0]
                        this_end_date = this_sb_dict[this_rb][this_side][this_sec][this_sb]['tds'][-1]
                        this_total_time = parse(this_end_date) - parse(this_start_date)
                        if this_len > 0:
                            this_pie_vals = []
                            for this_sfdv in [0, 1, 2, 3, 4, 5]:
                                this_sfd_tds = copy.deepcopy(this_sb_dict[this_rb][this_side]
                                                             [this_sec][this_sb]['tds'])
                                this_sfd_tds = [parse(x) for x in this_sfd_tds]
                                if this_sfd_tds[-1] < parse(end_date).replace(tzinfo=dt.timezone.utc):
                                    this_sfd_tds.append(parse(end_date).replace(tzinfo=dt.timezone.utc))
                                this_sfd_vals = copy.deepcopy(this_sb_dict[this_rb][this_side]
                                                              [this_sec][this_sb]['curr'])
                                this_pie_inds = [this_i for this_i, x in enumerate(this_sfd_vals) if x == this_sfdv]
                                this_tds = []
                                for this_i in this_pie_inds:
                                    if this_i + 1 < len(this_sfd_tds):
                                        this_st = this_sfd_tds[this_i]
                                        this_et = this_sfd_tds[this_i + 1]
                                        this_t = min(this_total_time, abs(this_et - this_st))
                                        this_tds.append(this_t)
                                if len(this_tds) > 0:
                                    this_pie_avg = np.sum(this_tds) / this_total_time
                                else:
                                    this_pie_avg = 0
                                this_pie_vals.append(this_pie_avg)
                            this_pie_vals.append(1 - np.sum(this_pie_vals))
                        else:
                            this_pie_vals = [0, 0, 0, 0, 0, 0, 1]

                        this_plt_dict[this_side][this_sec][this_sb]['pie'] = this_pie_vals

            for this_side in this_plt_dict:
                this_max_v = int(this_sb_dict[this_rb]["max_v"] / 2)
                this_max_h = int(this_sb_dict[this_rb]["max_h"] / 2)
                this_fig, this_ax = plt.subplots(nrows=this_max_v,
                                                 ncols=this_max_h + 1)
                this_fig.set_size_inches(11.5, 7)
                this_fig.suptitle(this_rb + " " + this_side + " SFD Values", size="xx-large", weight="extra bold")
                this_patches = [mpatches.Patch(color=c) for c in this_cs]
                for r in this_ax:
                    for c in r:
                        c.axis('off')
                plt.subplots_adjust(wspace=0, hspace=0)
                for this_sec in this_plt_dict[this_side]:
                    this_sec_hs = sorted(list(set([int(this_plt_dict[this_side][this_sec][x]["hpos"] / 2)
                                                   for x in this_plt_dict[this_side][this_sec]])))
                    this_ax[0, this_sec_hs[0]].annotate(this_sec, [0, 0], [0, 1.7], "axes fraction", size='large',
                                                        weight='bold')
                    for this_sb in this_plt_dict[this_side][this_sec]:
                        this_sb_vals = this_plt_dict[this_side][this_sec][this_sb]
                        this_hpos = int(this_sb_vals["hpos"] / 2)
                        if this_sb_vals["vpos"] > 0:
                            this_vpos = int(this_sb_vals["vpos"] / 2 - 1)
                        else:
                            this_vpos = 0
                        this_sb_ax = this_ax[this_vpos, this_hpos]
                        this_plt_vals = [max(0, round(x, 3)) for x in this_sb_vals["pie"]]
                        this_sb_ax.set_title(this_sb, weight='bold', pad=4, ha='center', va='bottom')
                        this_sb_ax.margins(0)
                        this_sb_avg = this_plt_dict[this_side][this_sec][this_sb]["avg"]
                        this_sb_ax.pie(this_plt_vals, colors=this_cs, radius=1.4)
                        try:
                            this_sb_str = "Avg: {:02.2f}".format(this_sb_avg)
                        except:
                            this_sb_str = "No Values"
                        this_sb_ax.annotate(this_sb_str, [0, 0], [0, -0.3],
                                            "axes fraction",
                                            ma="center")
                        if this_plt_vals[-1] > 0:
                            this_sb_ax.title.set_bbox(dict(edgecolor=c_config['CIRe'],
                                                           boxstyle=mpatches.BoxStyle('Round4'),
                                                           fill=False,
                                                           lw=2))
                this_fig.legend(labels=[0, 1, 2, 3, 4, "Error"],
                                handles=this_patches,
                                fontsize="large",
                                loc='center left')
                this_fig_name = (this_dir + "\\" + this_site + "\\" + this_site + "_" + this_rb + "_" + this_side
                                 + "_SFD.png")
                this_fig.savefig(this_fig_name, dpi=300, transparent=True)
                this_fig_dict[this_rb].append(this_fig_name)

    return this_fig_dict


def get_mcr(this_rb):
    this_db = site_db[this_site]
    this_rb_srch_str = r"search/query?q=name:" + this_rb + r"&scope=af:\\AM01-01-SV-502" + "\\" + this_db + "\\" + this_site
    this_rb_ele = requests.get(piwebapi_base_srch + this_rb_srch_str,
                               verify=False,
                               auth=piwebapi_user).json()["Items"][0]
    this_sb_mcr_att = [x for x in this_rb_ele["Attributes"] if x["Name"] == "Sootblowing_Steam_MCR"][0]
    this_sb_mcr_webid = this_sb_mcr_att["WebId"]
    this_tar_webid = [x["WebId"] for x in this_sb_mcr_att["Attributes"] if x["Name"] == "Target"][0]
    this_mcr = [x for x in this_rb_ele["Attributes"] if x["Name"] == "MCR"][0]["Value"]
    this_sb_mcr_srch_str = "streams/" + this_sb_mcr_webid + "/summary?StartTime=" + start_date + "&EndTime=" \
                           + end_date + "&SummaryType=Average"
    this_sb_mcr = requests.get(piwebapi_base_srch + this_sb_mcr_srch_str,
                               auth=piwebapi_user,
                               verify=False).json()["Items"][0]["Value"]["Value"]
    this_tar_srch_str = "streams/" + this_tar_webid + "/recorded?StartTime=01/01/1970"
    this_tar = requests.get(piwebapi_base_srch + this_tar_srch_str,
                            auth=piwebapi_user,
                            verify=False).json()["Items"][0]["Value"]

    return this_sb_mcr, this_tar, this_mcr


def gen_pptx(this_site, this_ops_plots, this_perf_plots, this_zone_plots, this_flow_plots):
    #   Make sure the Ppt isnt making duplicate slides/images
    for this_rb in this_ops_plots:
        try:
            os.remove(r"C:\Users\William.Machamer.CI\OneDrive - Clyde Industries Inc\Documents" + r"\Testing" + "\\" +
                      this_site + "\\" + this_site + "_PPR_" + end_date + ".pptx")
        except:
            pass

        #   Generate PPTX with CI base as master slides
        this_prs = pptx.Presentation(r"C:\Users\William.Machamer.CI\OneDrive - Clyde Industries Inc\Documents"
                                     r"\Clyde_Industries_Powerpoint_Template.pptx")

        #   Pull in master slide layouts
        ppt_master_slides = {}
        for i in range(len(this_prs.slide_layouts)):
            ppt_master_slides[this_prs.slide_layouts[i].name] = this_prs.slide_layouts[i]

        #   Add title and date
        this_title_slide = this_prs.slides[0]
        this_title_slide.shapes[0].text = this_site + " " + this_rb + " Peak Performance Report"
        this_title_slide.shapes[1].text = end_date

        #   Add an Executive summary slide
        es_slide = this_prs.slides.add_slide(ppt_master_slides["1_Content"])
        es_slide.name = "Executive Summary"
        es_slide.shapes[0].text = "Executive Summary"

        this_avg_mcr, this_tar, this_mcr = get_mcr(this_rb)

        this_body_shape = es_slide.shapes.placeholders[1]
        this_tf = this_body_shape.text_frame
        this_tf.text = "Steam Usage"

        if type(this_avg_mcr) == float:
            this_p = this_tf.add_paragraph()
            this_p.text = "{:02.2f} %MCR daily average steam usage".format(this_avg_mcr)
            this_p.level = 1

            this_p = this_tf.add_paragraph()
            this_p.text = "{:02.2f} %MCR target (based on MCR of {})".format(float(this_tar), int(this_mcr))
            this_p.level = 1

        stm_ops_slide = this_prs.slides.add_slide(ppt_master_slides["No_Content"])
        stm_ops_slide.name = "Steam and Ops"
        stm_ops_slide.shapes[0].text = "Steam Usage and Call Type"
        stm_ops_slide.shapes.add_picture(this_ops_plots[this_rb][0], pptx.util.Inches(0.5), pptx.util.Inches(1))

        this_perf_count = 1
        for this_plot in this_perf_plots[this_rb]:
            mst_slide = this_prs.slides.add_slide(ppt_master_slides["No_Content"])
            mst_slide.name = "PerfPlots" + str(this_perf_count)
            mst_slide.shapes[0].text = "Performance Plots"
            mst_slide.shapes.add_picture(this_plot, pptx.util.Inches(0.5), pptx.util.Inches(1))
            this_perf_count += 1

        for this_zone in this_zone_plots[this_rb]:
            this_z_name = this_zone[-8:-4]
            this_slide = this_prs.slides.add_slide(ppt_master_slides["No_Content"])
            this_slide.name = this_z_name
            this_slide.shapes[0].text = this_z_name
            this_slide.shapes.add_picture(this_zone, pptx.util.Inches(0.2), pptx.util.Inches(0.2))
        if len(this_flow_plots) > 0:
            for this_sec in this_flow_plots[this_rb]:
                this_name = this_sec.split("_")[3]
                this_slide = this_prs.slides.add_slide(ppt_master_slides["No_Content"])
                this_slide.name = this_name
                this_slide.shapes[0].text = this_name + " Sootblower Flows"
                this_slide.shapes.add_picture(this_sec, pptx.util.Inches(0.5), pptx.util.Inches(1))
        if len(this_amp_plots) > 0:
            for this_sec in this_amp_plots[this_rb]:
                this_name = this_sec.split("_")[3]
                this_slide = this_prs.slides.add_slide(ppt_master_slides["No_Content"])
                this_slide.name = this_name
                this_slide.shapes[0].text = this_name + " Sootblower Current"
                this_slide.shapes.add_picture(this_sec, pptx.util.Inches(0.5), pptx.util.Inches(1))

        this_prs.save(r"C:\Users\William.Machamer.CI\OneDrive - Clyde Industries Inc\Documents" + r"\Testing" + "\\" +
                      this_site + "\\" + this_site + " " + this_rb + "_PPR_" + str(
            dt.datetime.today().date()) + ".pptx")


# this_dir = r"C:\Users\William.Machamer.CI\OneDrive - Clyde Industries Inc\Documents\Testing"
# start_date = "12/01/2020"
# end_date = "12/15/2020"
# this_site = "Eastover"
# this_rb = "RB1"
# this_opt = "ops"  # ops or sd
# check_for_site_folder(this_site)
# this_ops_dict = get_ops_dict(this_site)
# this_zone_dict, this_z_config = get_zones_dict(this_site, this_rb, start_date, end_date)
# this_zce_dict = get_zce_dict(this_site, this_z_config, this_rb, start_date, end_date)
# this_perf_dict = get_perf_dict(this_site)
# this_flow_dict = get_flow_dict(this_site, this_rb, start_date, end_date)
# this_sfd_dict = get_sfd_dict(this_site)
# this_amp_dict = get_amp_dict(this_site)
# this_amp_plots = get_amp_plots(this_amp_dict, this_opt)
# this_ops_plots = get_stm_ops_plot(this_site, this_ops_dict)
# this_perf_plots = get_perf_plots(this_perf_dict)
# this_zone_plots = get_zones_plots(this_zce_dict, this_zone_dict, this_z_config)
# this_flow_plots = get_flow_plots(this_flow_dict, this_opt)
# this_sfd_plots = get_sfd_plot(this_sfd_dict)
# plt.close("all")
# gen_pptx(this_site, this_ops_plots, this_perf_plots, this_zone_plots, this_flow_plots)
